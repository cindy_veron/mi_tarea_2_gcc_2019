#!/usr/bin/python
# -*- coding: latin-1 -*-
import os

def populate():
    # Se crean algunos usuarios
    
    add_usuario('peed1', 'pabloiinfpuna@hotmail.com', 'admin', 'Pablo ','Escobar')
    add_usuario('peed2', 'UNA130215@hotmail.com', 'admin', 'Elias', 'Duarte')
    add_usuario('peed3', 'ramonlocore@hotmail.com', 'admin', 'Ramon', 'Ayala')
    

    #Se crean algunos Roles de Sistema
    add_rol_sistema('Administrador')
    add_rol_sistema('Predeterminado')
    add_rol_sistema('Scrum Master')
    #add_rol_sistema('Developers')

    #Permisos
    perm_pred=[ 
                'add_logentry',
                'change_logentry',
                'delete_logentry',
                'view_logentry',
                'add_session',
                'change_session',
    ]

    perm_adm=[
                'add_logentry',
                'change_logentry',
                'delete_logentry',
                'view_logentry',
                'add_permission',
                'change_permission',
                'delete_permission',
                'view_permission',
                'add_group',
                'change_group',
                'delete_group',
                'view_group',
                'add_user',
                'change_user',
                'delete_user',
                'view_user', 
                'add_contenttype',
                'change_contenttype',
                'delete_contenttype',
                'view_contenttype',
                'add_session',
                'change_session',
                'add_proyecto'
    ]
    
    perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
            ]
    return (perm_pred, perm_adm, perm_scrum)
    """
    Asigna permisos a Rol de sistema
    """
def asign_perms_sys(perm_pred, perm_adm, perm_scrum):
    #Permisos para Scrum
    for i in perm_scrum:
        try:
            grupo = Group.objects.get(name="Scrum Master")
            permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))
    
    #Permisos para Administrador
    for i in perm_adm:
        try:
            grupo = Group.objects.get(name="Administrador")
            permiso = Permission.objects.get(codename=i)
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))

    #Permisos para Predeterminado
    for i in perm_pred:
        try:
            grupo = Group.objects.get(name="Predeterminado")
            permiso = Permission.objects.get(codename=i)
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))

    """
    usuario1=User.objects.get(username='peed1')
    usuario2=User.objects.get(username='peed2')
    asign_rol_sys_user('Administrador',usuario1)
    asign_rol_sys_user('Predeterminado',usuario1)
    asign_rol_sys_user('Scrum Master',usuario2)
    """



"""
Agrega usuario
"""
def add_usuario(username, correo, contrasena, nombre, apellido):
    new_user = User.objects.create_user(username, correo, contrasena)
    new_user.first_name = nombre
    new_user.last_name = apellido
    new_user.save()

"""
Agrega rol de sistema
"""
def add_rol_sistema(nombre_rol):
    Group.objects.get_or_create(name=nombre_rol)



"""
    Asigna Rol a usuario
"""
def asign_rol_sys_user(nombre_rol_sistema, usuario):
     grupo = Group.objects.get(name=nombre_rol_sistema)   
     print(usuario)
     grupo.user_set.add(usuario)
   


if __name__ == '__main__':
    print("Inicio de la población de la base de datos de desarrollo")
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project01.settings')

    import django
   
    django.setup()
    from django.contrib.auth.models import User, Group, Permission
    from django.db.models import Q
    
   
    perm_pred, perm_adm, perm_scrum = populate()
    asign_perms_sys(perm_pred, perm_adm, perm_scrum)