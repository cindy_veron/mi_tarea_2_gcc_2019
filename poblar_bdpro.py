import os


def populate():
    
    # Se crean algunos usuarios
    add_usuario('admin', 'walterjr_14@hotmail.com', 'admin', 'Administrado', 'Sistema', '1234567', '09046969',
                'Administrador del sistema', 'Asunción', 8, 1)
    add_usuario('chrismtz', 'christian.verga2@gmail.com', 'admin', 'Christian', 'Verga', '3881235', '0984437411',
                'Scrum Master', 'Luque', 8, 2)
    add_usuario('walter', 'waldojr.is2@gmail.com', 'walterjr', 'Walter', 'Jara', '4796281', '0983579483', 'asd',
                'Asuncion', 7, 2)
    add_usuario('gerardo', 'gerar2.maldona2.2@gmail.com', 'kiki', 'Gerardo', 'Maldonado', '4474973', '0981887712',
                'asdasd', 'Capiata', 7, 2)



def add_usuario(username, correo, contrasena, nombre, apellido, cedula, telefono, descripcion, direccion,
                horas_laborales, rol):
    new_user = User.objects.create_user(username, correo, contrasena)
    new_user.first_name = nombre
    new_user.last_name = apellido
    new_user.usuario.ci = cedula
    new_user.usuario.telefono = telefono
    new_user.usuario.descripcion = descripcion
    new_user.usuario.direccion = direccion
    new_user.usuario.horas_laborales = horas_laborales
    rolcito = Rol2.objects.get(id=rol)
    new_user.usuario.id_rol = rolcito

    new_user.save()


if __name__ == '__main__':
    print("Inicio de la población de la base de datos de desarrollo")
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'gestorpro.settings')

    import django

    django.setup()
    from usuario.models import Usuario
    
    populate()
