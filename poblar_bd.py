#!/usr/bin/python
# -*- coding: latin-1 -*-
import os

def populate():
    # Se crean algunos usuarios
    #add_usuario('peed1', 'pabloiinfpuna@hotmail.com', 'admin', 'Pablo ','Escobar')
    #add_usuario('peed2', 'pabloiinfpuna@hotmail.com', 'admin', 'Elias', 'Duarte')
    #add_usuario('peed3', 'pabloiinfpuna@hotmail.com', 'admin', 'Ramon', 'Ayala')

    #Se crean algunos Roles de Sistema
    
    add_rol_sistema('Administrador')
    add_rol_sistema('Predeterminado')
    add_rol_sistema('Developers')
    add_rol_sistema('Scrum Master')

    #Permisos
    perm_pred=['add_logentry',
                'change_logentry',
                'delete_logentry',
                'view_logentry',
                'add session',
                'change session',
                'crear_proyecto'
    ]

    perm_adm=['add_logentry',
                'change_logentry',
                'delete_logentry',
                'view_logentry',
                'add permission',
                'change permission',
                'delete permission',
                'view permission',
                'add group',
                'change group',
                'delete group',
                'view group',
                'add user',
                'change user',
                'Can delete user',
                'view user', 
                'add content type',
                'change content',
                'delete content',
                'view content',
                'add session',
                'Can change session',
                'crear_proyecto'
    ]
    
    perm_scrum=['agregarMiembro_proyecto',
                'darBajaMiembro_proyecto',
                'delete_Integrante',
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificarRolDeMiembro_proyecto',
                'verListaMiembro_proyecto',
                'view_proyecto',
                'change_proyecto',
                'delete_proyecto',
                'agregarMiembroSprint_proyecto',
                'darBajaMiembroSprint_proyecto',
                'modificarRolDeMiembro_proyecto',
                'verListaMiembro_proyecto',
                'modificarRolDeMiembroSprint_proyecto',
                'verListaMiembroSprint_proyecto',
                'crearRol_proyecto',
                'modificarRol_proyecto',
                'eliminarRol_proyecto',
                'consultarRrol_proyecto',
                'crearTUS_proyecto',
                'modificarTUS_proyecto',
                'eliminarTUS_proyecto',
                'consultarTUS_proyecto',
                'importarTUS_proyecto',
                'verProducBacklog_proyecto',
                'crearUS_proyecto',
                'modificarUS_proyecto',
                'eliminarUS_proyecto',
                'consultarUS_proyecto',
                'crearSprint_proyecto',
                'modificarSprint_proyecto',
                'consultarSprint_proyecto',
                'add_roldeproyecto',
                'change_roldeproyecto',
                'delete_roldeproyecto',
                'view_roldeproyecto',
                'add_actividad',
                'change_actividad',
                'delete_actividad',
                'view_actividad',
                'add_tipous',
                'change_tipous',
                'delete_tipous',
                'view_tipous',
                'listar_tipous',
                'add_us',
                'change_us',
                'delete_us',
                'view_us'
            ]
    """
    Asigna permisos a Rol de sistema
    """
    #Permisos para Scrum
    for i in perm_scrum:
        try:
            grupo = Group.objects.get(name="Scrum Master")
            permiso = Permission.objects.get(codename=i)
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))
        
    #Permisos para Administrador
    for i in perm_adm:
        try:
            grupo = Group.objects.get(name="Administrador")
            permiso = Permission.objects.get(codename=i)
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))

      

    #Permisos para Predeterminado
    for i in perm_adm:
        try:
            grupo = Group.objects.get(name="Predeterminado")
            permiso = Permission.objects.get(codename=i)
            grupo.permissions.add(permiso)
        except:
            print("Fail "+ str(i))
       
  
    """
    usuario1=User.objects.get(username='peed1')
    usuario2=User.objects.get(username='peed2')
    asign_rol_sys_user('Administrador',usuario1)
    asign_rol_sys_user('Predeterminado',usuario1)
    asign_rol_sys_user('Scrum Master',usuario2)
    """

   


"""
Agrega usuario
"""
def add_usuario(username, correo, contrasena, nombre, apellido):
    new_user = User.objects.create_user(username, correo, contrasena)
    new_user.first_name = nombre
    new_user.last_name = apellido
    new_user.save()

"""
Agrega rol de sistema
"""
def add_rol_sistema(nombre_rol):
    Group.objects.get_or_create(name=nombre_rol)



"""
    Asigna Rol a usuario
"""
def asign_rol_sys_user(nombre_rol_sistema, usuario):
     grupo = Group.objects.get(name=nombre_rol_sistema)   
     print(usuario)
     grupo.user_set.add(usuario)
   


if __name__ == '__main__':
    print("Inicio de la población de la base de datos de desarrollo")
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project01.settings')

    import django
   
    django.setup()
    from django.contrib.auth.models import User, Group, Permission
    
   
    populate()