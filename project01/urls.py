"""project01 URL Configuration

La lista `urlpatterns` enruta las URL a las vistas. Para más información por favor vea:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Ejemplos:
Vistas de funciones
    1. Agregar una importación: desde las vistas de importación my_app
    2. Agregue una URL a urlpatterns: ruta ('', views.home, name = 'home')
Vistas basadas en clase
    1. Agregue una importación: desde other_app.views import Inicio
    2. Agregue una URL a urlpatterns: path ('', Home.as_view (), name = 'home')
Incluyendo otro URLconf
    1. Importe la función include (): desde django.urls import include, path
    2. Agregue una URL a urlpatterns: ruta ('blog /', include ('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth.views import LoginView, logout_then_login, PasswordResetView, PasswordResetDoneView, \
    PasswordResetConfirmView, PasswordResetCompleteView

from project01.views import index

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', index, name = 'index'),
    path('usuario/',include('apps.usuario.urls',namespace='usuario')),
    path('us/',include('apps.us.url',namespace='us')),
    path('rol/', include(('apps.rol.urls','rol'), namespace='rol')),
    path('proyecto/', include('apps.proyecto.urls', namespace='proyecto')),


    path('',LoginView.as_view(),{'template_name':'templates/registration/login.html'}, name='login'),
    path('logout/',logout_then_login, name='logout'),
    url(r'^reset/password_reset',PasswordResetView.as_view(),{'template_name':'registration/password_reset_form.html','email_template_name':'registration/password_reset_email.html'}, name='password_reset'),
    path('reset/password_reset_done',PasswordResetDoneView.as_view(),{'template_name':'registration/password_reset_done'}, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',PasswordResetConfirmView.as_view(),{'template_name':'registration/password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'reset/done',PasswordResetCompleteView.as_view(),{'template_name':'registration/password_reset_complete.html'},name='password_reset_complete'),
]

""""
    se tienen las direcciones de:
    -usuario
    -rol
    -Login
    -Recuperacion de contraseña que consta de:
    
    *password reset done: envia un correo, con un enlace para reestablecer la contraseña, al correo que el usuario proporciona 
    *password reset email: mensaje que contiene el correo recibido con el enlace
    *password reset confirm: proporciona el reestablecimiento, de contraseña una vez accedido al enlace
    *password reset complete: indica al usuario que la contraseña se reestablecio correctamente
"""
