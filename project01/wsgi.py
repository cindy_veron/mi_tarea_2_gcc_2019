"""
Configuración SGI para el proyecto project01.

Expone el WSGI invocable como una variable de nivel de módulo llamada `` application``.

Para más información sobre este archivo, vea
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project01.settings')

application = get_wsgi_application()
