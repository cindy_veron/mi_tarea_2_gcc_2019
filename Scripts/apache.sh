#!/bin/bash
###
#$1:
#$2:
#$3:
#$4:
#$6:
#$7:
#$8:
#$9:
#$10:
###


#ACTUALIZACIÓN DE REPOSITORIO
#sudo apt update

#INSTALACIÓN DE APACHE v.2.4
#apt install apache2

#INSTALACIÓN DE LIBRERIA PARA APACHE
#sudo apt install libapache2-mod-wsgi-py3



echo $1

#VARIABLES
nombre_carpeta="DESARROLLO"
nombre_proyecto="project01"
usuario="pablo-escobar"
nombre_entorno="venv-is2"
#dir_inicio=/home/pablo-escobar/DESARROLLO
dir_inicio="/home/$usuario/$nombre_carpeta"
#dir_produccion=/home/pablo-escobar/DESARROLLO/is2
dir_produccion="$dir_inicio/is2"
#dir_entorno=/home/pablo-escobar/DESARROLLO/venv-is2
dir_entorno="$dir_inicio/$nombre_entorno"
nombre_bd="bd_desarrollo"


#CONFIGURAR PROYECTO DJANGO CON APACHE
apache_config="
Alias /static $dir_produccion/static/
<Directory $dir_produccion/static>
	Require all granted
</Directory>
<Directory $dir_produccion/$nombre_proyecto>
	<Files wsgi.py>
		Require all granted
	</Files>
</Directory>

WSGIDaemonProcess $nombre_proyecto python-home=$dir_entorno python-path=$dir_produccion
WSGIProcessGroup $nombre_proyecto
WSGIScriptAlias / $dir_produccion/$nombre_proyecto/wsgi.py

"
echo $apache_config
sudo sh -c "echo \"<VirtualHost *:80>$apache_config</VirtualHost>\" > /etc/apache2/sites-available/000-default.conf"

sudo a2enmod wsgi
sudo a2ensite 000-default.conf
/etc/init.d/apache2 restart 

sudo service apache2 reload
