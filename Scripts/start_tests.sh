#! /bin/bash
#Retroceder una carpeta
cd ..

clear
cd ..
source venv-is2/bin/activate
cd is2/
echo "##################################"
echo "####Ejecuta el test para roles####"
echo "##################################"



#Ejecutar el proceso
echo "PRUEBA UNITARIA PARA ROL"
#Proceso exitoso de agregar un rol de sistema
python3 manage.py test apps.rol.tests.Test_registrar.registrar_exitoso
#Proceso fallido de agregar un rol de sistema
python3 manage.py test apps.rol.tests.Test_registrar.registrar_fallido
#Proceso exitoso de listar roles de sistema
python3 manage.py test apps.rol.tests.Test_listar.listar_exitoso
#Proceso fallido de listar roles de sistema
python3 manage.py test apps.rol.tests.Test_listar.listar_fallido
#Proceso exitoso de eliminar un rol del sistema
python3 manage.py test apps.rol.tests.Test_eliminar.eliminar_exitoso
#Proceso fallido de eliminar un rol del sistema
python3 manage.py test apps.rol.tests.Test_eliminar.eliminar_fallido
#Proceso exitoso de modificar un rol del sistema
python3 manage.py test apps.rol.tests.Test_modificar.modificar_exitoso
#Proceso fallido de modificar un rol del sistema
python3 manage.py test apps.rol.tests.Test_modificar.modificar_fallido


echo "PRUEBA UNITARIA PARA USUARIO"
#Proceso exitoso de agregar un usuario al sistema
python3 manage.py test apps.usuario.tests.Test_registrar.registrar_exitoso
#Proceso fallido de agregar un usuario al sistema
python3 manage.py test apps.usuario.tests.Test_registrar.registrar_fallido
#Proceso exitoso de modificar un usuario al sistema
python3 manage.py test apps.usuario.tests.Test_update.modificar_exitoso
#Proceso fallido de modificar un usuario al sistema
python3 manage.py test apps.usuario.tests.Test_update.modificar_fallido
#Proceso fallido de listar usuarios de sistema
python3 manage.py test apps.usuario.tests.Test_listar.listar_exitoso
#Proceso exitoso de eliminar un usuario del sistema
python3 manage.py test apps.usuario.tests.Test_eliminar.eliminar_exitoso
#Proceso fallido de eliminar un usuario del sistema
python3 manage.py test apps.usuario.tests.Test_eliminar.eliminar_fallido

echo "PRUEBA UNITARIA DE PROYECTO"
#Proceso exitoso de iniciar sesion al sistema
python3 manage.py test apps.proyecto.tests.Test_registrar.login_exitoso
#Proceso exitoso de agregar un PROYECTO al sistema
python3 manage.py test apps.proyecto.tests.Test_registrar.registrar_exitoso
#Proceso fallido de agregar un PROYECTO al sistema
python3 manage.py test apps.proyecto.tests.Test_registrar.registrar_fallido
#Proceso exitoso de modificar un PROYECTO al sistema
python3 manage.py test apps.proyecto.tests.Test_modificar.modificar_exitoso
#Proceso fallido de modificar un PROYECTO al sistema
python3 manage.py test apps.proyecto.tests.Test_modificar.modificar_fallido
#Proceso exitoso de eliminar un PROYECTO al sistema
python3 manage.py test apps.proyecto.tests.Test_eliminar.eliminar_exitoso
#Proceso fallido de eliminar un PROYECTO al sistema
python3 manage.py test apps.proyecto.tests.Test_eliminar.eliminar_fallido

echo "PRUEBA UNITARIA PARA TIPO DE USER STORY"
#Proceso exitoso de agregar un tipoUS con sus respectivas actividades al sistema
python3 manage.py test apps.tipous.tests.Test_registrar.registrar_exitoso
#Proceso fallido de agregar un tipoUS con sus respectivas actividades al sistema
python3 manage.py test apps.tipous.tests.Test_registrar.registrar_fallido
#Proceso exitoso de modificar un tipoUS con sus respectivas actividades al sistema
python3 manage.py test apps.tipous.tests.Test_modificar.modificar_exitoso
#Proceso fallido de modificar un tipoUS con sus respectivas actividades al sistema
python3 manage.py test apps.tipous.tests.Test_modificar.modificar_fallido
#Proceso exitoso de eliminar un tipoUS con sus respectivas actividades al sistema
python3 manage.py test apps.tipous.tests.Test_eliminar.eliminar_exitoso
#Proceso fallido de eliminar un tipoUS con sus respectivas actividades al sistema
python3 manage.py test apps.tipous.tests.Test_eliminar.eliminar_fallido
#Proceso exitoso de listar un tipoUS con sus respectivas actividades al sistema
python3 manage.py test apps.tipous.tests.Test_listar.listar_exitoso
#Proceso fallido de listar un tipoUS con sus respectivas actividades al sistema
python3 manage.py test apps.tipous.tests.Test_listar.listar_fallido




echo "PRUEBA UNITARIA DE USER STORY"
#Proceso exitoso de agregar un US al sistema
python3 manage.py test apps.us.tests.Test_registrar.registrar_exitoso
#Proceso fallido de agregar un US al sistema
python3 manage.py test apps.us.tests.Test_registrar.registrar_fallido
#Proceso exitoso de modificar un US al sistema
python3 manage.py test apps.us.tests.Test_modificar.modificar_exitoso
#Proceso fallido de modificar un US al sistema
python3 manage.py test apps.us.tests.Test_modificar.modificar_fallido
#Proceso exitoso de eliminar un US al sistema
python3 manage.py test apps.us.tests.Test_eliminar.eliminar_exitoso
#Proceso fallido de eliminar un US al sistema
python3 manage.py test apps.us.tests.Test_eliminar.eliminar_fallido