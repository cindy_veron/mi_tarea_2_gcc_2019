from django.urls import path,include
from django.conf.urls import url,include
from apps.nota.views import notaUS, notaList


app_name = 'nota'

urlpatterns = [
    url(r'^crear_nota$', notaUS.as_view(), name='crear_nota'),
    url(r'^listar_nota$',notaList.as_view(), name='listar_nota'),

]