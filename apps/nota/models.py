from django.db import models
from apps.us.models import US

class NotaUs(models.Model):
    """
        Clase Nota US:
        Registra un nombre,una
        descripcion , y horas trabajadas
        para la nota y lo asocia a un
         User Story especifico.
    """
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    horas = models.PositiveIntegerField(default=0)
    archivo_adjunto = models.FileField(default=None, blank=True)
    us = models.ForeignKey(US, on_delete=models.SET_NULL, null=True)
