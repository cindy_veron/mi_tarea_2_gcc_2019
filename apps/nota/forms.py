from django import forms
from apps.nota.models import  NotaUs

"""
    Formulario:

    hereda de forms.Modelform

    Class Meta:
    se especifican los campos que seran que
    seran añadidos o modificados:
    * nombre
    * descripcion
    * horas
    * archivo_adjunto
"""


class FormNotas(forms.ModelForm):
    class Meta:
        model = NotaUs
        fields =[
            'nombre',
            'descripcion',
            'horas',
            'archivo_adjunto',
        ]
        labels={
            'nombre':'Nombre de la Nota',
            'descripcion':'Descripcion',
            'horas':'Horas Trabajadas',
            'archivo_adjunto':'Archivo Adjunto',
        }
        widgets ={
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control'}),
            'horas': forms.TextInput(attrs={'class': 'form-control'}),
            'archivo_adjunto':forms.FileInput(),
        }
