from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
from django.urls import reverse_lazy
from apps.tipous.forms import ActividadForm, ActividadFormSet
from apps.tipous.models import TipoUS, Actividad
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from guardian.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from apps.proyecto.models import Proyecto
from django.shortcuts import get_object_or_404
from apps.us.models import US

@login_required()
def verifica_permiso_crear(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.crear_tus')
    print("h TUS crear")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.crear_tus', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return TipoUsActividadCrear.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

@login_required()
def verifica_permiso_modificar(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.modificar_tus')
    print("h TUS modificar")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.modificar_tus', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return TipoUsActividadActualizar.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

@login_required()
def verifica_permiso_eliminar(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.eliminar_tus')
    print("h TUS eliminar")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.eliminar_tus', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return EliminarTipoUS.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


# === ListarTipoUS ===
# class ListarTipoUS(PermissionRequiredMixin,ListView):
class ListarTipoUS(ListView):
    # @login_required
    # permission_required = 'tipous.view_tipous'
    """
    Clase Listar TipoUS
    **Hereda de:**
    1. PermissionRequiredMixin: CBV mixin que 
    verifica que el usuario actual tiene todos 
    los permisos especificados
    2. ListView: Renderiza alguna lista de objetos,
    seteado por `self.model` or `self.queryset`.
    `self.queryset` puede ser cualquier iterable 
    de items no solo un queryset
    **Retorna:** url a ListarTipoUS
     """

    model = TipoUS
    template_name = "tipous/tus_listar.html"


    # permission_required = 'auth.change_user'

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs.get('pk'))
        return TipoUS.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListarTipoUS, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
        return context



"""
    Funcion Atividad por TipoUS
    se encarga de enviar 
    todos los datos almacenados de tipo de US y 
    las actvidades correspondientes, para que luego 
    en el html se pueda filtrar las actividades 
    correspondientes por tipo de Us
"""


"""
    Clase Crear Actividad de TipoUS
    **Hereda de:**
    1. CreateView: Renderiza alguna lista de objetos,
    seteado por `self.model` or `self.queryset`.
    `self.queryset` puede ser cualquier iterable de 
    items no solo un queryset
    **Retorna:** url a ListarTipoUS
    
    Se sobreescriben los metodos get_cotext_data,
    form_valid para mantener los datos en estado
    correcto
"""
class TipoUsActividadCrear(CreateView):
    permission_required = 'tipous.add_tipous'
    model = TipoUS
    fields = [
        'nombre',
        'descripcion',
    ]
    template_name = "tipous/tus_agregar.html"

    # success_url = reverse_lazy('tus:listar')
    def get_context_data(self, **kwargs):
        context = super(TipoUsActividadCrear, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
        if self.request.POST:
            context['actividades'] = ActividadFormSet(self.request.POST)
        else:
            context['actividades'] = ActividadFormSet()
        return context

    def form_valid(self, form):
        tus = form.save(commit=False)
        proyecto = Proyecto.objects.get(pk=self.kwargs.get('pk'))
        tus.proyecto = proyecto
        context = self.get_context_data()
        actividades = context['actividades']
        with transaction.atomic():
            self.object = form.save()
            if actividades.is_valid():
                actividades.instance = self.object
                actividades.save()
        return super(TipoUsActividadCrear, self).form_valid(form)

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.object.proyecto.id
        return reverse_lazy("proyecto:tipous:listar", kwargs={'pk': l})

"""
    Clase Actualizar Actividad de TipoUS
    **Hereda de:**
    1. UpdateView: Renderiza alguna lista de objetos,
    seteado por `self.model` or `self.queryset`.
    `self.queryset` puede ser cualquier iterable de 
    items no solo un queryset
    **Retorna:** url a ListarTipoUS

    Se sobreescriben los metodos get_cotext_data,
    form_valid para mantener los datos en estado
    correcto
"""


class TipoUsActividadActualizar(UpdateView):
    permission_required = 'tipous.change_tipous'
    model = TipoUS
    fields = [
        'nombre',
        'descripcion',
    ]
    template_name = "tipous/tus_agregar.html"

    # success_url = reverse_lazy('tus:listar')
    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identificación del user story a ser modificado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['pp'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        data = super(TipoUsActividadActualizar, self).get_context_data(**kwargs)
        data['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
        if self.request.POST:
            data['actividades'] = ActividadFormSet(self.request.POST, instance=self.object)
        else:
            data['actividades'] = ActividadFormSet(instance=self.object)
        print("lista de us")
        print( US.objects.filter(tipo_us=self.kwargs['pp']))
        data['us_list'] = US.objects.filter(tipo_us=self.kwargs['pp'])
        return data

    def form_valid(self, form):
        tus = form.save(commit=False)
        proyecto = Proyecto.objects.get(pk=self.kwargs.get('pk'))
        tus.proyecto = proyecto
        context = self.get_context_data()
        actividades = context['actividades']
        with transaction.atomic():
            self.object = form.save()
            if actividades.is_valid():
                actividades.instance = self.object
                actividades.save()
        return super(TipoUsActividadActualizar, self).form_valid(form)

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.kwargs.get('pk')
        # print(l)
        return reverse_lazy('proyecto:tipous:listar', kwargs={'pk': l})

"""
    Clase Eliminar TipoUS
    **Hereda de:**
    1. DeleteteView: Renderiza alguna lista de objetos,
    seteado por `self.model` or `self.queryset`.
    `self.queryset` puede ser cualquier iterable de 
    items no solo un queryset
    **Retorna:** url a ListarTipoUS
"""


# ===== ELIMINAR ROL ====
class EliminarTipoUS(DeleteView):
    permission_required = 'tipous.delete_tipous'
    model = TipoUS
    template_name = 'tipous/tus_delete.html'

    # success_url = reverse_lazy('tus:listar')
    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del user story a ser eliminado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['pp'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de miembros de team,
        ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
        """
        context = super(EliminarTipoUS, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
        context['us_list'] = US.objects.filter(tipo_us=self.kwargs['pp'])
        return context

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.kwargs.get('pk')
        # print(l)
        return reverse_lazy('proyecto:tipous:listar', kwargs={'pk': l})















