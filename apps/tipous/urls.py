from django.conf.urls import url
#from apps.tipous.views import ListarTipoUS,TipoUsActividadCrear,EliminarTipoUS,TipoUsActividadActualizar
from apps.tipous.views import verifica_permiso_eliminar,verifica_permiso_modificar,verifica_permiso_crear, ListarTipoUS
app_name='tipous'
"""
urlpatterns = [
    url(r'^listar/',ListarTipoUS.as_view(),name='listar'),
    url(r'^registrar/', TipoUsActividadCrear.as_view(), name='crear'),
    url(r'^eliminar/(?P<pp>\d+)/$',EliminarTipoUS.as_view(),name='eliminar'),
    url(r'actualizar/(?P<pp>[0-9]+)/$', TipoUsActividadActualizar.as_view(), name='actualizar'),
    # url(r'^detalles/', ListarActividadPorTipoUS.as_view(), name='detalles'),
    #url(r'^detalles/', ListarActividadPorTipoUS, name='detalles'),
]
"""
urlpatterns = [
    url(r'^listar/',ListarTipoUS.as_view(),name='listar'),
    url(r'^registrar/', verifica_permiso_crear, name='crear'),
    url(r'^eliminar/(?P<pp>\d+)/$',verifica_permiso_eliminar,name='eliminar'),
    url(r'actualizar/(?P<pp>[0-9]+)/$', verifica_permiso_modificar, name='actualizar'),
    # url(r'^detalles/', ListarActividadPorTipoUS.as_view(), name='detalles'),
    #url(r'^detalles/', ListarActividadPorTipoUS, name='detalles'),
]