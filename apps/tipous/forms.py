from apps.tipous.models import TipoUS,Actividad
from django import forms
from django.forms.models import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm

#PARA LISTAR
"""
    Formulario:
    parametros:
    1. hereda de forms.Modelform
    
    Se sobreescribe el metodo __init__
    para que el metodo padre tenga en cuenta
    los argumentos que le son enviados
    
    Class Meta:
    se especifican los campos que seran que
    seran añadidos o modificados:
    * nombre
    
    Se especifica que el formulario sera
    seteado en linea, para los cambios sobre
    actividades ya sea añadir o eliminar, en
    las Clases crear o modificar Acividades de 
    TipoUS
"""

#PARA CREAR Y MODIFICAR
class ActividadForm(forms.ModelForm):
    #Para ponerle clases a todos los elementos visibles de este formulario, que servirá para ponerle estilos
    def __init__(self, *args, **kwargs):
        super(ActividadForm,self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class']='input_actividad'

    class Meta:
        model=Actividad
        fields =[
            'nombre',
        ]
        exclude = ()


ActividadFormSet = inlineformset_factory(TipoUS,Actividad,form=ActividadForm, extra=1)
       
