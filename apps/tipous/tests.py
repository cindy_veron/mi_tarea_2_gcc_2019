from django.test import TestCase
from django.test import Client
from apps.tipous.models import TipoUS, Actividad
from django.contrib.auth.models import User,Group,Permission
from apps.proyecto.models import Proyecto

"""
    Esta clase se encarga de realizar prueba unitaria para crear tipo de user Story con sus respectivas actividades
    En caso exitoso retorna 302
    En caso fallido retorna 200
"""
class Test_registrar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def registrar_exitoso(self):
        print('PROCESO DE REGISTRAR TIPO DE USER STORY - EXITOSO')
        #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################



        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })


                                                            
        salida=response.status_code
        var = TipoUS.objects.all()
        print('Si el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,302)

    def registrar_fallido(self):
        print('PROCESO DE REGISTRAR TIPO DE USER STORY - FALLIDO')
        #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################



        response =  self.client.post('/proyecto/1/tipous/registrar/',data={'actividad_set-TOTAL_FORMS':3,
                                                                    'actividad_set-INITIAL_FORMS':0,
                                                                    'actividad_set-MIN_NUM_FORMS':0,
                                                                    'actividad_set-MAX_NUM_FORMS':1000,
                                                                    'actividad_set-0-id':'',
                                                                    'actividad_set-0-tipoUS':'',
                                                                    'actividad_set-0-nombre':'ACTIVIDAD',
                                                                    'actividad_set-0-DELETE':'',
                                                                    })
        salida=response.status_code
        var = TipoUS.objects.all()
        print('Si el registro de tipo US no fue válido, que es lo que se espera, se retornará 0')
        print(var)
        print(salida)
        self.assertEquals(salida,200)





      

"""
    Esta clase se encarga de realizar prueba unitaria para modificar tipo de user Story con sus respectivas actividades
    En caso exitoso retorna 302
    En caso fallido retorna 200
"""
class Test_modificar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')

    def modificar_exitoso(self):
        print('PROCESO DE REGISTRAR TIPO DE USER STORY - EXITOSO')
       #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })


                                                            
        salida=response.status_code
        var = TipoUS.objects.all()
        print('Si el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,302)
     #######################PROCESO DE MODIFICAR TIPO US#########################
        print("ANTES...")
        print(Actividad.objects.all())


        response =  self.client.post('/proyecto/1/tipous/actualizar/1/', data={ 'nombre':'TUS',
                                                                                'descripcion':'TIPO DE USER STORY',
                                                                                'actividad_set-TOTAL_FORMS':3,
                                                                                'actividad_set-INITIAL_FORMS':0,
                                                                                'actividad_set-MIN_NUM_FORMS':0,
                                                                                'actividad_set-MAX_NUM_FORMS':1000,
                                                                                'actividad_set-0-id':'',
                                                                                'actividad_set-0-tipoUS':'',
                                                                                'actividad_set-0-nombre':'CAMPO ACTIVIDAD1',
                                                                                'actividad_set-0-DELETE':'',
                                                                                'actividad_set-1-id':'',
                                                                                'actividad_set-1-tipoUS':'',
                                                                                'actividad_set-1-nombre':'CAMPO ACTIVIDAD2',
                                                                                'actividad_set-1-DELETE':'',
                                                                                'actividad_set-2-id':'',
                                                                                'actividad_set-2-tipoUS':'',
                                                                                'actividad_set-2-nombre':'CAMPO ACTIVIDAD3',
                                                                                'actividad_set-2-DELETE':'',
                                                                                'actividad_set-3-id':'',
                                                                                'actividad_set-3-tipoUS':'',
                                                                                'actividad_set-3-nombre':'CAMPO ACTIVIDAD4',
                                                                                'actividad_set-4-id':'',
                                                                                'actividad_set-4-tipoUS':'',
                                                                                'actividad_set-4-nombre':'CAMPO ACTIVIDAD5',	
                                                                                'actividad_set-5-id':'',
                                                                                'actividad_set-5-tipoUS':'',
                                                                                'actividad_set-5-nombre':'CAMPO ACTIVIDAD6',	
                                                                })

        print("DESPUES...")
        print(Actividad.objects.all())

        ##VALIDACIÓN
        print('Si modificar tipo US fue válido, que es lo que se espera, se retornará 1')
        salida=response.status_code
        var = TipoUS.objects.all().count()
        print(var)
        print(salida)
        self.assertEquals(salida,302)


    def modificar_fallido(self):
        print('PROCESO DE MODIFICAR TIPO DE USER STORY - FALLIDO')
       #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })


                                                            
        salida=response.status_code
        var = TipoUS.objects.all()
        print('Si el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,302)
     #######################PROCESO DE MODIFICAR TIPO US#########################
        print("ANTES...")
        print(Actividad.objects.all())

        print("INTENTO ACTUALIZAR UN REGISTRO BORRANDO EL NOMBRE Y LA DESCRIPCIÓN DEL TIPO US")
        response =  self.client.post('/proyecto/1/tipous/actualizar/1/', data={ 'actividad_set-TOTAL_FORMS':3,
                                                                                'actividad_set-INITIAL_FORMS':0,
                                                                                'actividad_set-MIN_NUM_FORMS':0,
                                                                                'actividad_set-MAX_NUM_FORMS':1000,
                                                                                'actividad_set-0-id':'',
                                                                                'actividad_set-0-tipoUS':'',
                                                                                'actividad_set-0-nombre':'CAMPO ACTIVIDAD1',
                                                                                'actividad_set-0-DELETE':'',
                                                                                'actividad_set-1-id':'',
                                                                                'actividad_set-1-tipoUS':'',
                                                                                'actividad_set-1-nombre':'CAMPO ACTIVIDAD2',
                                                                                'actividad_set-1-DELETE':'',
                                                                                'actividad_set-2-id':'',
                                                                                'actividad_set-2-tipoUS':'',
                                                                                'actividad_set-2-nombre':'CAMPO ACTIVIDAD3',
                                                                                'actividad_set-2-DELETE':'',
                                                                                'actividad_set-3-id':'',
                                                                                'actividad_set-3-tipoUS':'',
                                                                                'actividad_set-3-nombre':'CAMPO ACTIVIDAD4',
                                                                                'actividad_set-4-id':'',
                                                                                'actividad_set-4-tipoUS':'',
                                                                                'actividad_set-4-nombre':'CAMPO ACTIVIDAD5',	
                                                                                'actividad_set-5-id':'',
                                                                                'actividad_set-5-tipoUS':'',
                                                                                'actividad_set-5-nombre':'CAMPO ACTIVIDAD6',	
                                                                })

        print("DESPUES...")
        print(Actividad.objects.all())

        ##VALIDACIÓN
        print('Si modificar tipo US no fue válido, que es lo que se espera, se retornará 0')
        salida=response.status_code
        var = TipoUS.objects.all().count()
        print(var)
        print(salida)
        self.assertEquals(salida,200)


        
        

"""
    Esta clase se encarga de realizar prueba unitaria para eliminar tipo de user Story con sus respectivas actividades
    En caso exitoso retorna 302
    En caso fallido retorna 404
"""
class Test_eliminar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def eliminar_exitoso(self):
        print('PROCESO DE ELIMINAR TIPO DE USER STORY - EXITOSO')
       #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })


                                                            
        salida=response.status_code
        var = TipoUS.objects.all()
        print('Si el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,302)
     #######################PROCESO DE MODIFICAR TIPO US#########################
        print("ANTES...")
        print(TipoUS.objects.all())
        response=self.client.post('/proyecto/1/tipous/eliminar/1/')
        print("DESPUES...")
        print(TipoUS.objects.all())
        salida=response.status_code
        var = TipoUS.objects.all().count()
        print('Si el proceso de eliminar el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,302)

    #Cuando se le pasa un id que no existe
    def eliminar_fallido(self):
        print('PROCESO DE MODIFICAR TIPO DE USER STORY - FALLIDO')
        #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })


                                                            
        salida=response.status_code
        var = TipoUS.objects.all()
        print('Si el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,302)
     #######################PROCESO DE MODIFICAR TIPO US#########################
        print("ANTES...")
        print(TipoUS.objects.all())
        response=self.client.post('/proyecto/1/tipous/eliminar/')
        print("DESPUES...")
        print(TipoUS.objects.all())
        salida=response.status_code
        var = TipoUS.objects.all().count()
        print('Si el proceso de eliminar el registro de tipo US no fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,404)
"""
    Esta clase se encarga de realizar prueba unitaria para listar tipo de user Story con sus respectivas actividades
    En caso exitoso retorna 302
    En caso fallido retorna 404
"""
class Test_listar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def listar_exitoso(self):
        print('PROCESO DE LISTAR TIPO DE USER STORY - EXITOSO')
         #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })


                                                            
        salida=response.status_code
        var = TipoUS.objects.all()
        print('Si el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,302)
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        response =self.client.post('/tipous/registrar/', data={'nombre':'TUS',
                                                                'descripcion':'TIPO DE USER STORY',
                                                                'actividad_set-TOTAL_FORMS':3,
                                                                'actividad_set-INITIAL_FORMS':0,
                                                                'actividad_set-MIN_NUM_FORMS':0,
                                                                'actividad_set-MAX_NUM_FORMS':1000,
                                                                'actividad_set-0-id':'',
                                                                'actividad_set-0-tipoUS':'',
                                                                'actividad_set-0-nombre':'ACTIVIDAD',
                                                                'actividad_set-0-DELETE':'',
                                                                })
        response=self.client.post('/proyecto/1/tipous/listar/')
        print(TipoUS.objects.all())
        salida=response.status_code
        var = TipoUS.objects.all().count()
       # print('Si el proceso de listar el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,405)

    def listar_fallido(self):
        print('PROCESO DE LISTAR TIPO DE USER STORY - FALLIDO')
         #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })


                                                            
        salida=response.status_code
        var = TipoUS.objects.all()
        print('Si el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,302)
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        response =self.client.post('/tipous/registrar/', data={'nombre':'TUS',
                                                                'descripcion':'TIPO DE USER STORY',
                                                                'actividad_set-TOTAL_FORMS':3,
                                                                'actividad_set-INITIAL_FORMS':0,
                                                                'actividad_set-MIN_NUM_FORMS':0,
                                                                'actividad_set-MAX_NUM_FORMS':1000,
                                                                'actividad_set-0-id':'',
                                                                'actividad_set-0-tipoUS':'',
                                                                'actividad_set-0-nombre':'ACTIVIDAD',
                                                                'actividad_set-0-DELETE':'',
                                                                })
        response=self.client.post('/proyecto/1/tipous/lista/')
        print(TipoUS.objects.all())
        salida=response.status_code
        var = TipoUS.objects.all().count()
       # print('Si el proceso de listar el registro de tipo US fue válido, que es lo que se espera, se retornará 1')
        print(var)
        print(salida)
        self.assertEquals(salida,404)