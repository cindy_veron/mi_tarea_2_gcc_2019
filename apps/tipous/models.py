from django.db import models
from apps.proyecto.models import Proyecto

class TipoUS(models.Model):
    """
    Clase TipoUS:
    Registra un nombre y una lista de Actividades
    """
    nombre=models.CharField(max_length=50, unique=True)
    descripcion=models.CharField(max_length=150,null=True)
    proyecto = models.ForeignKey(Proyecto, null=True, blank=True, on_delete=models.CASCADE)
    
    class Meta:
        """
        Meta-Clase:
        Establece los parametros de ordenamiento de los objetos de la clase.
        """
        ordering=['id']

    def __str__(self):
        return self.nombre
    
    class Meta:
        permissions = (("listar_tipous", "retorna la visualización de la lista de tipo US"),)  
    


# === Actividad ===
class Actividad(models.Model):
    """
    Clase Actividad:
    Registra un nombre,un orden y lo asocia a un tipo de user story
    """
    nombre=models.CharField(max_length=50)
    #Una actividad se relaciona con un Tipo de User Story {Relación varias actividades a un TUS} 
    tipoUS=models.ForeignKey(TipoUS,on_delete=models.CASCADE)
   
    def __str__(self):
        return self.nombre
    
   