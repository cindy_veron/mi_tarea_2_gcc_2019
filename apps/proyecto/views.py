from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group, User
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, render_to_response, get_object_or_404

from apps.miembros.models import Miembro
from apps.sprint.models import Sprint
from .models import Proyecto
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from .forms import UpadateForm
from django.urls import reverse_lazy

from apps.tipous.models import TipoUS, Actividad
from apps.us.models import US
from apps.proyecto.models import Proyecto

# Create your views here.
"""
funcion verifica_permisos de detalle:
* si el miembro esta autorizado se le 
da acceso ver el contenido del proyecto
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_detalle(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.ver_proyecto')
    print("h proyecto detalle")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.ver_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return proyecto.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
funcion verifica_permisos de listar:
* si el miembro esta autorizado se le 
da acceso ver la lista de proyectos
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_listar(request,*args,**kwargs):
    #has_permission = request.user.has_perm('proyecto.view_proyecto')
    print("h1")
   # print(has_permission)
    # has_permission|= request.user.has_perm('proyecto.view_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    #if has_permission:
    print("entro")
    return proyectoList.as_view()(request, *args, **kwargs)
   # else:
    #    return HttpResponse("<html><p>No posee permisos</p></html>")

"""
funcion verifica_permisos de crear:
* si el miembro esta autorizado se le 
da acceso crear proyectos
* si no tiene esos permisos se le
 informa que no posee permisos

"""

@login_required()
def verifica_permiso_crear(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.add_proyecto')
   # has_permission|= request.user.has_perm('proyecto.change_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return RegistroProyecto.as_view()(request, *args, **kwargs)
    else:
       return HttpResponse("<html><p>No posee permisos</p></html>")

"""
funcion verifica_permisos de modificar:
* si el miembro esta autorizado se le 
da acceso a modificar atributos del proyecto
* si no tiene esos permisos se le
 informa que no posee permisos
"""

@login_required()
def verifica_permiso_modificar(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.modificar_proyecto')
    print("h proyecto modificar")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.modificar_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return ProyectoUpdate.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
funcion verifica_permisos de eliminar:
* si el miembro esta autorizado se le 
da acceso a eliminar proyecto
* si no tiene esos permisos se le
 informa que no posee permisos

"""

@login_required()
def verifica_permiso_eliminar(request, *args, **kwargs):
    has_permission = request.user.has_perm('proyecto.eliminar_proyecto')
    print("h proyecto eliminar")
    print(has_permission)
    has_permission |= request.user.has_perm('proyecto.eliminar_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return ProyectoDelete.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
Clase Detalle de Proyecto el miembro que esta 
autorizado puede ver el detalle del proyecto 

ademas se modifican el metodo 
get_context_data para personalizar
la vista
"""
class proyecto(DetailView):
    model = Proyecto
    template_name = 'proyecto/index.html'

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        context['miembro'] = Miembro.objects.filter(proyecto=context['proyecto'])
        return context



"""
Clase Listar Proyecto el miembro que esta 
autorizado puede agregar miembros cada 
usuario puede ver los proyectos al
 cual pertenece

ademas se modifica el metodo get object
para personalizar la lista de proyectos

"""
class proyectoList(ListView):
    model = Proyecto
    template_name = 'proyecto/proyecto_list.html'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['ide'])
        obj = queryset.get()
        return obj

"""
Clase Listar Proyecto el usuario 
que tiene los permisos correspondiente 
puede registrar un nuevo proyecto

ademas se modifica el metodo form_valid
para personalizar la asignacion del 
usuario scrum master

"""

class RegistroProyecto(CreateView):
    model = Proyecto
    template_name = "proyecto/registrar.html"
    form_class = UpadateForm
    success_url = reverse_lazy('proyecto:listar')

    def form_valid(self, form):
        response = super(RegistroProyecto, self).form_valid(form)
        scrum=form.cleaned_data['scrum']
        usuario=User.objects.get(username=scrum)
        rolProyecto_ScrumMaster = Group.objects.get(name="Scrum Master")
        Miembro.objects.create(usuario=usuario, rolProyecto=rolProyecto_ScrumMaster, proyecto=self.object)
        return response


"""
Clase Actualizar Proyecto el miembro que esta 
autorizado puede modificar el proyecto
en al cual pertenece

"""

class ProyectoUpdate(UpdateView):
    model = Proyecto
    template_name = "proyecto/editar.html"
    form_class = UpadateForm
    success_url = reverse_lazy('proyecto:listar')


"""
Clase Eliminar Proyecto el miembro que esta 
autorizado puede eliminar proyecto

"""

class ProyectoDelete(DeleteView):
    model = Proyecto
    template_name = 'proyecto/proyecto_delete.html'
    success_url = reverse_lazy('proyecto:listar')

"""
funcion verifica_permisos de ver kanban:
* si el miembro esta autorizado se le 
da acceso ver el contenido del tablero
kanban
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def ver_kanban(request, *args, **kwargs):
    has_permission = request.user.has_perm('proyecto.ver_kanban')
    print("h kanban ver")
    print(has_permission)
    has_permission |= request.user.has_perm('proyecto.consultar_sprint', Proyecto.objects.get(id=kwargs['pk']))
    print("h kanban consultar sprint")
    print(has_permission)
    if has_permission:
        return Kanban.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
Clase kanban el miembro que esta 
autorizado puede ver el tablero
kanban

ademas se modifica el metodo 
get_context_data para personalizar
los elementos para la vista

"""

class Kanban(DetailView):
    model = Proyecto
    template_name = "Tablero kanban/kanban.html"

    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el tablero
        """
        context = super(Kanban,self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        sprint_list = Sprint.objects.filter(proyecto=context['proyecto'])
        context['sprint_list'] = sprint_list

        #user story dentro de un proyecto
        us_list = US.objects.filter(Q(proyecto=context["proyecto"]) & (Q(estado_us=1) | Q(estado_us=0) | Q(estado_us=2)))

        context['us_list'] = us_list
        #sprint_list = Sprint.objects.filter(proyecto=context['proyecto'])

        #cuenta la cantidad de us
        contador=us_list.count()

        #contiene la lista de todos los TUS temporal
        lista = TipoUS.objects.filter(proyecto=context['proyecto'])

        #contiene la lista de todos los TUS persistente
        tus_list = TipoUS.objects.filter(proyecto=context['proyecto'])

        #recorre los us y excluye de la lista TUS temporal todos los TUS que fueron asinados a los us
        for i in range(0,contador):
            lista=(lista.exclude(id=us_list[i].tipo_us.id))

        #recorre la lista de TUS temporal y de la lista de TUS persistente excluye dichos TUS temporales
        for i in lista:
            tus_list=(tus_list.exclude(id=i.id))

        print('tus_list')
        print(tus_list)

        context['tus_list'] = tus_list
        return context

"""
Clase Cambiar Fase el miembro que esta 
autorizado puede cambiar la fase en
la cual se encuentra un user story

ademas se modifica el metodo get succes
url para personalizar y mantener
en un estado valido a los us

"""
class CambiarFase_der(UpdateView):
    model = US
    fields = ['actividad_actual','estado_kanban']
    #template_name = "proyectos/ver_tablero.html"


    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        usid = self.kwargs["uid"]
        us = US.objects.get(id=usid)
        mayor = -1
        for fase in us.tipo_us.actividad_set.all():
            if fase.id > mayor:
                mayor = fase.id

        if(us.actividad_actual.id == mayor and us.estado_kanban == 2):
            us.estado_us = 2
            us.save()

        pr=self.kwargs['pk']
        return reverse_lazy("proyecto:kanban",kwargs = {'pk': pr})

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['uid'])
        obj = queryset.get()
        return obj

"""
Clase Cambiar Estado, el miembro que esta 
autorizado puede cambiar el estado 
del Proyecto 

ademas se modifica el metodo get succes
url para personalizar el proyecto

"""
class CambiarEstado(UpdateView):
    model = Proyecto
    fields = ['estado']

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        usid = self.kwargs["pk"]
        proyecto = Proyecto.objects.get(id=usid)
        proyecto.save()
        return reverse_lazy("proyecto:index",kwargs = {'pk': proyecto.id})

    # def get_object(self, queryset=None):
    #     if queryset is None:
    #         queryset = self.get_queryset()
    #     queryset = queryset.filter(id=self.kwargs['uid'])
    #     obj = queryset.get()
    #     return obj



"""
funcion verifica_permisos de cambiar Usuario:
* si el miembro esta autorizado se le 
da acceso cambiar el usario asignado a un
user story en el tablero kanban
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_cabiarUsuario(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.ver_proyecto')
    print("h proyecto detalle")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.ver_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return cambiarUsuario.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
Clase Cambiar Usuario miembro que esta 
autorizado puede cambiar el usuario 
asignado a un user story estando en 
el tablero kanban

ademas se modifica el metodo get object,
get context data y get succes url
para personalizar la lista de miembros
user story, usuario asignado en base
al proyecto

"""
class cambiarUsuario(UpdateView):
    model = US
    fields = ['team_member', ]
    template_name = 'Tablero kanban/cambiar_Usuario.html'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data(**kwargs)

        #obtiene el proyecto
        proyecto = Proyecto.objects.get(id=self.kwargs['pk'])

        #obtiene el sprint del proyecto
        sprint = Sprint.objects.filter(proyecto=self.kwargs['pk'])

        #lista de los miembros del sprint
        miembro_list = sprint[0].sprintTeam

        #obtiene el user story
        user_story = US.objects.get(id=self.kwargs['tmid'])

        #obtiene el usuario relacionado al user story
        usuario_actual=user_story.team_member

        #de la lista de miembros excluye usuario actual
        miembro_list = miembro_list.exclude(id=usuario_actual.id)

        context['usuario_actual']= usuario_actual
        context['miembro_list']= miembro_list
        context['user_story']=user_story
        context['proyecto']=proyecto

        return context

    def get_success_url(self):
        l = self.kwargs.get('pk')
        return reverse_lazy('proyecto:kanban', kwargs={'pk': l})

class finalizar_us(UpdateView):
    model = US
    fields = ['estado_us']
    template_name = "proyecto/finalizar_us.html"

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        usid = self.kwargs["tmid"]
        print("usid")
        print(usid)
        pr= self.kwargs["pk"]
        print("PROYECTO")
        print(pr)
        story = US.objects.get(id=usid)
        story.estado_us=3
        story.save()
        return reverse_lazy("proyecto:kanban",kwargs = {'pk': pr})

class listarUSfinalizados(ListView):
    """
    Menu de listado de user stories finalizados del proyecto.
    **Hereda de:**

    1. ListView: Renderiza alguna lista de objetos,seteado por
    `self.model` or `self.queryset`.`self.queryset` puede ser cualquier
    iterable de items no solo un queryset

    **Retorna:** url a proyecto
     """
    # `Extends:` ListView; para la visualizacion de los user stories.
    model = US
    template_name = 'us/us_finalizados.html'
    paginate_by = 10

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return US.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        us_list = US.objects.filter(proyecto=context['proyecto'], estado_us=3)
        context['us_list'] = us_list
        return context

