from django.test import TestCase
from django.test import Client
from apps.tipous.models import TipoUS, Actividad
from django.contrib.auth.models import User,Group,Permission
from apps.proyecto.models import Proyecto

"""
    Esta clase se encarga de realizar prueba unitaria para crear un proyecto 
    En caso exitoso retorna 302
    En caso fallido retorna 200
    Primero se crea un usuario con permisos de administrador
    Luego se crea un proyecto
    por ultimo se crea user story perteneciente a dicho proyecto
"""


class Test_registrar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def login_exitoso(self):
        print('PROCESO DE INICIAR SESION - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)

    def registrar_exitoso(self):
        print('PROCESO DE REGISTRAR NUEVO PROYECTO - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        var = response.status_code
        cantidad = Proyecto.objects.all()
        print("Proceso correcto...")
        print(cantidad)
        self.assertEquals(var, 302) 

    def registrar_fallido(self):
        print('PROCESO DE REGISTRAR NUEVO PROYECTO - FALLIDO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':2,
                                                                })
        var = response.status_code
        cantidad = Proyecto.objects.all()
        print("Proceso incorrecto... \nUSUARIO ELEGIDO PARA SCRUM ES EL SUPERUSUARIO")
        print(cantidad)
        self.assertEquals(var, 200) 






"""
    Esta clase se encarga de realizar prueba unitaria para eliminar un proyecto 
    En caso exitoso retorna 302
    En caso fallido retorna 200
    Primero se crea un usuario con permisos de administrador
    Luego se crea un proyecto
    por ultimo se crea user story perteneciente a dicho proyecto
"""

class Test_eliminar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def eliminar_exitoso(self):
        print('PROCESO DE ELIMINAR NUEVO PROYECTO - EXITOSO')
        #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE ELIMINAR PROYECTO#########################
        print("ANTES...")
        print(cantidad)
        response =  self.client.post('/proyecto/eliminar/1')
        var = response.status_code
        cantidad = Proyecto.objects.all()
        print("DESPUES...")
        print(cantidad)
        print("Proceso correcto...")
        self.assertEquals(var, 302) 
    
    def eliminar_fallido(self):
        print('PROCESO DE ELIMINAR NUEVO PROYECTO - FALLIDO')
        #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE ELIMINAR PROYECTO#########################
        print("ANTES...")
        print(cantidad)
        response =  self.client.post('/proyecto/eliminar/')
        var = response.status_code
        cantidad = Proyecto.objects.all()
        print("DESPUES...")
        print(cantidad)
        print("Proceso incorrecto... No especificar el proyecto a eliminarse")
        self.assertEquals(var, 404) 







"""
    Esta clase se encarga de realizar prueba unitaria para modificar un proyecto 
    En caso exitoso retorna 302
    En caso fallido retorna 200
    Primero se crea un usuario con permisos de administrador
    Luego se crea un proyecto
    por ultimo se crea user story perteneciente a dicho proyecto
"""
class Test_modificar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def modificar_exitoso(self):
        print('PROCESO DE MODIFICAR NUEVO PROYECTO - EXITOSO')
        #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        User.objects.create_user(username='nico', password='admin')
        usuario3=User.objects.get(id=4)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 y usuario3
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)
        grupo.user_set.add(usuario3)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE MODIFICAR PROYECTO#########################
        print("ANTES...")
        print(cantidad)
        response =  self.client.post('/proyecto/editar/1', data={'codigo':1,
                                                                   'nombre':'proyectoxxx',
                                                                   'descripcion':'descripcion proyecto xxx',
                                                                   'fechaInicio':'11/05/2019 23:07:33',
                                                                   'fechaFin':'11/05/2019 23:07:34',
                                                                   'scrum':4,
                                                                })
        var = response.status_code
        cantidad = Proyecto.objects.all()
        print("DESPUES...")
        print(cantidad)
        print("Proceso correcto...")
        self.assertEquals(var, 302)
        


    def modificar_fallido(self):
        print('PROCESO DE MODIFICAR NUEVO PROYECTO - FALLIDO')
        #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        User.objects.create_user(username='nico', password='admin')
        usuario3=User.objects.get(id=4)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 y usuario3
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)
        grupo.user_set.add(usuario3)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE MODIFICAR PROYECTO#########################
        print("ANTES...")
        print(cantidad)
        response =  self.client.post('/proyecto/editar/', data={'codigo':1,
                                                                   'nombre':'proyectoxxx',
                                                                   'descripcion':'descripcion proyecto xxx',
                                                                   'fechaInicio':'11/05/2019 23:07:33',
                                                                   'fechaFin':'11/05/2019 23:07:34',
                                                                   'scrum':4,
                                                                })
        var = response.status_code
        cantidad = Proyecto.objects.all()
        print("DESPUES...")
        print(cantidad)
        print("Proceso incorrecto...")
        self.assertEquals(var, 404) 
    