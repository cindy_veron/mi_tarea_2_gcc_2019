from django.urls import include, re_path
from django.conf.urls import url
from .views import verifica_permiso_listar, verifica_permiso_detalle, verifica_permiso_modificar, \
    verifica_permiso_eliminar, verifica_permiso_crear, ver_kanban, CambiarFase_der,verifica_permiso_cabiarUsuario,CambiarEstado,\
    finalizar_us,listarUSfinalizados
#from .views import proyecto, verifica_permiso_detalle, proyectoList, \
#   ProyectoUpdate, ProyectoDelete, ver_kanban, RegistroProyecto, verifica_permiso_listar
app_name = 'proyecto'

urlpatterns = [
    re_path(r'^listar/', verifica_permiso_listar, name='listar'),
    re_path(r'^registrar/', verifica_permiso_crear, name="registrar"),
    re_path(r'^(?P<pk>\d+)/detalle/$', verifica_permiso_detalle , name='index'),
    re_path(r'^editar/(?P<pk>\d+)$', verifica_permiso_modificar, name='editar'),
    re_path(r'^eliminar/(?P<pk>\d+)$', verifica_permiso_eliminar, name='eliminar'),

    url(r'^(?P<pk>\d+)/rolProyecto/', include('apps.rolDeProyecto.urls', namespace='rolProyecto')),
    url(r'^(?P<pk>\d+)/miembro/', include('apps.miembros.urls', namespace='miembro')),
    url(r'^(?P<pk>\d+)/us/', include('apps.us.url', namespace='us')),
    url(r'^(?P<pk>\d+)/tablero_Kanban/$', ver_kanban, name="kanban"),
    url(r'^(?P<pk>\d+)/tablero_Kanban/cambiar_usuario/(?P<tmid>\d+)$', verifica_permiso_cabiarUsuario, name="cambiarUsuario"),
    url(r'^(?P<pk>\d+)/sprint/', include('apps.sprint.urls', namespace='sprint')),
    url(r'^(?P<pk>\d+)/tipous/', include('apps.tipous.urls', namespace='tus')),

    url(r"^(?P<pk>\d+)/tablero/cambiar_fase/(?P<uid>\d+)$", CambiarFase_der.as_view(), name="cambiar_fase"),
    url(r"^(?P<pk>\d+)/cambiar_estado/", CambiarEstado.as_view(), name="cambiar_estado"),
    url(r"^(?P<pk>\d+)/finalizar_user_story/(?P<tmid>\d+)$", finalizar_us.as_view(), name="finalizar_us"),
    url(r"^(?P<pk>\d+)/Release_Backlog/$", listarUSfinalizados.as_view(), name="release_backlog"),
]

"""
urlpatterns = [
    re_path(r'listar/', verifica_permiso_listar, name='listar'),
    re_path(r'^registrar/', RegistroProyecto.as_view(), name="registrar"),
    re_path(r'^(?P<pk>\d+)/detalle/$', verifica_permiso_detalle , name='index'),
    re_path(r'^editar/(?P<pk>\d+)$', ProyectoUpdate.as_view(), name='editar'),
    re_path(r'^eliminar/(?P<pk>\d+)$', ProyectoDelete.as_view(), name='eliminar'),

    url(r'^(?P<pk>\d+)/rolProyecto/', include('apps.rolDeProyecto.urls', namespace='rolProyecto')),
    url(r'^(?P<pk>\d+)/miembro/', include('apps.miembros.urls', namespace='miembro')),
    url(r'^(?P<pk>\d+)/us/', include('apps.us.url', namespace='us')),
    url(r'^(?P<pk>\d+)/tablero Kanban/$', ver_kanban, name="kanban"),
    url(r'^(?P<pk>\d+)/sprint/', include('apps.sprint.urls', namespace='sprint')),
    url(r'^(?P<pk>\d+)/tus/', include('apps.tipous.urls', namespace='tus')),
]
"""