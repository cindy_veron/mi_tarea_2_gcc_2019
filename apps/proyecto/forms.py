from django.contrib.auth.models import User

from .models import Proyecto
from django import forms


"""
    Formulario de Actualizacion:
    se extiende de la calse forms.ModelsForm
    
    la seleccion de los estados posibles del Proyecto:
    Pendiente, Activo, Cancelado
    
    En la Clase Meta se da la posibilidad de modificar
    los siguientes campos:
    'codigo',
    'nombre',
    'descripcion',
    'fechaInicio',
    'fechaFin',
    'estado',
"""


class UpadateForm(forms.ModelForm):
    # estado = forms.ChoiceField(choices=(('pendiente','pendiente'),('activo','activo'),('cancelado','cancelado')))
    scrum = forms.ModelChoiceField(queryset=User.objects.filter(is_active=True,is_superuser=False).exclude(username='AnonymousUser'))

    class Meta:
        model = Proyecto
        fields = [
                'codigo',
                'nombre',
                'descripcion',
                'fechaInicio',
                'fechaFin',
                # 'estado',
                'scrum'
                ]
        labels = {
                'codigo': 'Codigo',
                'nombre': 'Nombre',
                'descripcion': 'Descripcion',
                'fechaInicio': 'FechaInicio',
                'fechaFin': 'FechaFin',
                # 'estado' : 'Estado',
                'scrum': 'Scrum Master'
                }

        widgets= {
            'fechaInicio' : forms.DateTimeInput(attrs= {'class': 'datetime-input'}),
            'fechaFin': forms.DateTimeInput(attrs= {'class': 'datetime-input'}),
            }

#
# class RegistroForm(forms.ModelForm):
#
#     class Meta:
#         model = Proyecto
# #         fields = [{ 'fechaFin': 'FechaFin', }]
# #
# #         # widgets= {
# #         #     # 'codigo' : forms.NumberInput(),
# #         #     # 'nombre': forms.(),
# #         #     # 'descripcion': forms.TimeField(),
# #         #     'fechaInicio' : forms.DateTimeInput(attrs= {'class': 'datetime-input'}),
# #         #     'fechaFin': forms.DateTimeInput(attrs= {'class': 'datetime-input'}),
# #         #     'username': forms.TextInput(attrs={'class':'form-control'}),
# #         #     'first_name': forms.TextInput(attrs={'class':'form-control'}),
# #         #     'last_name': forms.TextInput(attrs={'class':'form-control'}),
# #         #     #'email': forms.EmailField(attrs={'class':'form-control'}),
# #         # }
