from django.db import models

# Create your models here.
"""
En el modelo Proyecto se especifica todos
 los detalles del proyecto tal como: codigo,
  nombre, 
  descripcion, 
  fechaInicio, 
  FechaFin, 
  tiempo restante y 
  estado del proyecto
"""

class Proyecto(models.Model):

    codigo = models.PositiveIntegerField(verbose_name="Codigo")
    nombre = models.CharField(verbose_name="Nombre" , max_length=50)
    descripcion = models.TextField(verbose_name="Descripcion",max_length=50)
    fechaInicio = models.DateTimeField(verbose_name="Fecha de Inicio")
    fechaFin = models.DateTimeField(verbose_name="Fecha de Fin")
    #scrum = models.CharField(verbose_name="Scrum",max_length=20 ,null=True)
    tiempoRestante = models.IntegerField(verbose_name="Tiempo Restante",default=0)
    estado = models.TextField(verbose_name="Estado",max_length=20,default="Pendiente")

    # sprints
    # miembros
    # TipoUs
    # product Backlog

    def __str__(self):
        return self.nombre
    """
    En la clase Meta se especifican:
    el ordenamiento de referencia
    el vervose_name y
    el vervose_name_plural
    """
    class Meta:
        ordering = ["codigo"]
        verbose_name = "Proyecto"
        verbose_name_plural = "Proyectos"

        # * Para agregar nuevos permisos hacen un nuevo campo en permissions
        # * Para verificar el permiso de usuario dentro del proyecto, usar el codename, que es la primera columna
        #   de esta lista de permisos. Ej de uso: proyecto.cancelar_proyecto, proyecto.suspender_proyecto
        permissions = [
            # Permisos de Proyecto
            ('cancelar_proyecto', 'Puede Cancelar Proyecto'),
            ('suspender_proyecto', 'Puede Suspender Proyecto'),
            ('reanudar_proyecto', 'Puede Reanudar Proyecto'),
            ('ver_proyecto', 'Puede Ver Proyecto'),
            ('modificar_proyecto', 'Puede Modificar Proyecto'),
            ('eliminar_proyecto', 'Puede Eliminar Proyecto'),

            # Permisos de Miembro en Proyecto
            ('agregar_miembro_proyecto', 'Puede Agregar Miembro en el Proyecto'),
            ('eliminar_miembro_proyecto', 'Puede Dar de baja a Miembro del Proyecto'),
            ('modificar_miembro_proyecto', 'Puede Modificar Rol de Miembro en Proyecto'),
            ('verlista_miembro_proyecto', 'Puede Ver la lista de Miembro en Proyecto'),

            # Permisos de Miembro en Sprint
            ('agregar_miembro_sprint', 'Puede Agregar Miembro en el Sprint'),
            ('eliminar_miembro_sprint', 'Puede Dar de baja a Miembro del Sprint'),
            ('modificar_miembro_sprint', 'Puede Modificar Rol de Miembro en el Sprint'),
            ('verlista_miembro_sprint', 'Puede Ver la lista de Miembro en el Sprint'),

            # Permisos de Roles de Proyecto
            ('crear_rol_proyecto', 'Puede Crear Rol de Proyecto'),
            ('modificar_rol_proyecto', 'Puede Modificar Rol de Proyecto'),
            ('eliminar_rol_proyecto', 'Puede Eliminar Rol de Proyecto'),
            ('ver_rol_proyecto', 'Puede Ver Rol de Proyecto'),

            # Permisos de TUS en Proyecto
            ('crear_tus', 'Puede Crear TUS en Proyecto'),
            ('modificar_tus', 'Puede Modificar TUS en Proyecto'),
            ('eliminar_tus', 'Puede Eliminar TUS en Proyecto'),
            ('ver_tus', 'Puede Ver TUS en Proyecto'),
            ('importar_tus', 'Puede importar TUS en Proyecto'),

            # Permiso de ver Product Backlog
            ('ver_productBacklog', 'Puede Ver Produc Backlog del Proyecto'),

            # Permiso de ver Sprint Backlog
            ('ver_sprintBacklog', 'Puede Ver Sprint Backlog del Proyecto'),

            # Permiso de ver Kanban
            ('ver_kanban', 'Puede Ver kanban del Proyecto'),

            # Permiso de User Story
            ('crear_us', 'Puede Crear User Story'),
            ('modificar_us', 'Puede Modificar User Story'),
            ('eliminar_us', 'Puede Eliminar User Story'),
            ('ver_us', 'Puede Consultar User Story'),
            ('ver_us_finalizado', 'Puede Consultar Release Backlog'),
            ('detalle_us', 'Puede Ver Detalle de User Sory'),

            # Permiso de Sprint
            ('crear_sprint', 'Puede Crear Sprint en Proyecto'),
            ('modificar_sprint', 'Puede Modificar Sprint en Proyecto'),
            ('consultar_sprint', 'Puede Ver Sprint de Proyecto'),
            ('eliminar_sprint', 'Puede Eliminar Sprint de Proyecto'),
        ]


class permiso(models.Model):
    """
    Modelo de Permisos de Proyecto contiene los siguientes atributos.
    id: identificador por defecto autoincremental
    nombre: identificador del Permiso

    Se cuenta con los siguientes permisos por defecto
    incluye: - ABMV de Proyecto
             - ABMV de Miembro del Proyecto
             - ABMV de Rol de Proyecto
    """
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return ''.format(self.nombre)

    class Meta:

        # * Para agregar nuevos permisos hacen un nuevo campo en permissions
        # * Para verificar el permiso de usuario dentro del proyecto, usar el codename, que es la primera columna
        #   de esta lista de permisos. Ej de uso: proyecto.cancelar_proyecto, proyecto.suspender_proyecto
        default_permissions=()
        permissions = [
            #Permisos de Proyecto
            ('cancelar_proyecto', 'Puede Cancelar Proyecto'),
            ('suspender_proyecto', 'Puede Suspender Proyecto'),
            ('reanudar_proyecto', 'Puede Reanudar Proyecto'),
            ('ver_proyecto', 'Puede Ver Proyecto'),
            ('modificar_proyecto', 'Puede Modificar Proyecto'),
            ('eliminar_proyecto', 'Puede Eliminar Proyecto'),

            #Permisos de Miembro en Proyecto
            ('agregar_miembro_proyecto', 'Puede Agregar Miembro en el Proyecto'),
            ('eliminar_miembro_proyecto', 'Puede Dar de baja a Miembro del Proyecto'),
            ('modificar_miembro_proyecto', 'Puede Modificar Rol de Miembro en Proyecto'),
            ('verlista_miembro_proyecto', 'Puede Ver la lista de Miembro en Proyecto'),

            #Permisos de Miembro en Sprint
            ('agregar_miembro_sprint', 'Puede Agregar Miembro en el Sprint'),
            ('eliminar_miembro_sprint', 'Puede Dar de baja a Miembro del Sprint'),
            ('modificar_miembro_sprint', 'Puede Modificar Rol de Miembro en el Sprint'),
            ('verlista_miembro_sprint', 'Puede Ver la lista de Miembro en el Sprint'),

            #Permisos de Roles de Proyecto
            ('crear_rol_proyecto', 'Puede Crear Rol de Proyecto'),
            ('modificar_rol_proyecto', 'Puede Modificar Rol de Proyecto'),
            ('eliminar_rol_proyecto', 'Puede Eliminar Rol de Proyecto'),
            ('ver_rol_proyecto', 'Puede Ver Rol de Proyecto'),

            #Permisos de TUS en Proyecto
            ('crear_tus', 'Puede Crear TUS en Proyecto'),
            ('modificar_tus', 'Puede Modificar TUS en Proyecto'),
            ('eliminar_tus', 'Puede Eliminar TUS en Proyecto'),
            ('ver_tus', 'Puede Ver TUS en Proyecto'),
            ('importar_tus', 'Puede importar TUS en Proyecto'),

            #Permiso de ver Product Backlog
            ('ver_productBacklog', 'Puede Ver Produc Backlog del Proyecto'),

            #Permiso de ver Sprint Backlog
            ('ver_sprintBacklog', 'Puede Ver Sprint Backlog del Proyecto'),

            #Permiso de ver Kanban
            ('ver_kanban', 'Puede Ver kanban del Proyecto'),

            #Permiso de User Story
            ('crear_us', 'Puede Crear User Story'),
            ('modificar_us', 'Puede Modificar User Story'),
            ('eliminar_us', 'Puede Eliminar User Story'),
            ('ver_us', 'Puede Consultar User Story'),
            ('ver_us_finalizado', 'Puede Consultar Release Backlog'),
            ('detalle_us', 'Puede Ver Detalle de User Sory'),

            #Permiso de Sprint
            ('crear_sprint', 'Puede Crear Sprint en Proyecto'),
            ('modificar_sprint', 'Puede Modificar Sprint en Proyecto'),
            ('consultar_sprint', 'Puede Ver Sprint de Proyecto'),
            ('eliminar_sprint', 'Puede Eliminar Sprint de Proyecto'),
        ]