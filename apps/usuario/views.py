from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView , UpdateView , DeleteView
from django.contrib.auth.mixins import PermissionRequiredMixin

from apps.miembros.models import Miembro
from apps.usuario.forms import RegistroForm

# Create your views here.
@login_required
def index(request):
    """
    Metodo que da bienvenida  al ususario . una vez validado el login
    """
    return render(request, "usuario/bien.html")


# return HttpResponse("esta es una prueba de login de acceso")

"""
   Listas basadas en Clases, ListView. Esta vista contendra la lista de usuarios del sistema
"""


class UsuarioList(PermissionRequiredMixin,ListView):
    model = User
    template_name = 'usuario/usuario_list.html'
    permission_required = 'auth.change_user'

    def get_context_data(self, **kwargs):
        # sobreescibe el contexto
        context = super(ListView, self).get_context_data(**kwargs)
        # Crea una lista de usuarios filtrando solo a los activos
        # usuariosLista = User.objects.all().filter(is_active=True)
        usuariosLista = User.objects.all().order_by("id")
        #Excluye el usuario Anonimo que genera django o django-guardian
        usuariosLista = usuariosLista.exclude(username="AnonymousUser")
        # Si es un usuario normal le excluye la lista de los usuarios Administradores
        # if -- falta condicion de que el usuario normal no vea los administradores---
        #   for admin in User.objects.filter(groups__name="Administrador"):
        #        usuariosLista = usuariosLista.exclude(id=admin.id)
        # Sobreescribe contexto object_list con la lista de usuarios filtrada
        context['object_list'] = usuariosLista
        return context


"""
   Esta Vista coontendra El formulario para el registro de Usuarios del sistema
"""


class RegistroUsuario(PermissionRequiredMixin, CreateView ):

    model = User
    template_name = "usuario/registrar.html"
    form_class = RegistroForm
    permission_required = 'auth.add_user'

    def form_valid(self, form):
        """
        Para asignar el usuario a un Rol ya cargado
        **Parametros**
           1. self: referencia al objeto
           2. form: Formulario de validacion del usuario
           **Retorna:** response: Formulario Valido
           """
        response = super(RegistroUsuario, self).form_valid(form)
        username=form.cleaned_data['username']
        rol = form.cleaned_data['groups']
        print(rol)
        print(username)
        g = Group.objects.get(name=rol) #Retorno el rol del modelo
        g.user_set.add(self.object) #Agrego el rol al usuario
        return response

    def get_success_url(self):
        """
        **Parametros**
        1. self: referencia al objeto
        **Retorna:** url del ListarUsuario
        """
        return reverse_lazy('usuario:listar')


class UserUpdate(PermissionRequiredMixin,UpdateView):
    model = User
    template_name = "usuario/registrar.html"
    form_class = RegistroForm
    permission_required = 'auth.change_user'

    def form_valid(self, form):
        """
        Para asignar el usuario a un Rol ya cargado
        **Parametros**
           1. self: referencia al objeto
           2. form: Formulario de validacion del usuario
           **Retorna:** response: Formulario Valido
           """
        response = super(UserUpdate, self).form_valid(form)
        #Extraigo los datos que recibo del formulario
        username=form.cleaned_data['username']
        rol = form.cleaned_data['groups']
        print(rol)
        print(username)
        #Retorno el rol del modelo
        g = Group.objects.get(name=rol)
        #Elimino mi grupo asociado al usuario
        print("HOLA MUNDO")
        print(self.object)
        usuario = User.objects.get(username=username)
        usuario.groups.clear()
        #Agrego el nuevo rol al usuario
        g.user_set.add(self.object)
        return response

    success_url = reverse_lazy('usuario:listar')


"""class UserDelete(PermissionRequiredMixin,UpdateView):
    model = User
    template_name = 'usuario/usuario_delete.html'
    form_class = RegistroForm
    permission_required = 'auth.change_user'
    
    def form_valid(self, form):
        response = super(UserDelete, self).form_valid(form)
        #Extraigo los datos que recibo del formulario
        usuario=self.object
        print("Usuario: ",self.object)
        print("Antes...")
        print(usuario.is_active)
        print("Despues...")
        usuario.is_active=False
        print(usuario.is_active)
        return response

    success_url = reverse_lazy('usuario:listar')

"""



class UserDelete(PermissionRequiredMixin,DeleteView):
    model = User
    template_name = 'usuario/usuario_delete.html'
    permission_required = 'auth.delete_user'
    success_url = reverse_lazy('usuario:listar')
