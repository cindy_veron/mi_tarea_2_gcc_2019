from django.urls import path,include,reverse_lazy
from django.conf.urls import url
from apps.usuario.views import index, UsuarioList, RegistroUsuario , UserUpdate , UserDelete


app_name='administracion'

urlpatterns= (
    path('', index, name='index'),
    url(r'^listar/', UsuarioList.as_view(), name='listar'),
    url(r'^registrar/', RegistroUsuario.as_view(), name="registrar"),
    url(r'^editar/(?P<pk>\d+)/$',UserUpdate.as_view(),name='usuario_editar'),
    url(r'^eliminar/(?P<pk>\d+)/$',UserDelete.as_view(),name='usuario_eliminar'),
)


#path('',base),
