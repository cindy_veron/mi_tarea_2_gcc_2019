from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django import forms
from django.db.models import Q

"""
    Formulario:
    parametros:
    1. hereda de forms.Modelform
	
	groups: Filtra los grupos que seran
	utilizados en el sistema

    Class Meta:
    se especifican los campos que seran que
    seran añadidos o modificados:
	* username,
	* first_name,
	* last_name,
	* email,
	* groups,
"""
class RegistroForm(UserCreationForm):

	groups = forms.ModelChoiceField(queryset=Group.objects.filter(~Q(permissions__content_type__model="permiso")), required=True,widget=forms.Select(attrs={'class':'form-control'}))

	class Meta:
		model = User
		fields = [
			'username',
			'first_name',
			'last_name',
			'email',
			'groups',
			]
		labels = {
			'username': 'Nombre de usuario',
			'first_name': 'Nombres',
			'last_name': 'Apellidos',
			'email': 'Correo',
			'groups': 'Roles',
			}
			

	
	def save(self, commit=True):
		user = super(RegistroForm, self).save(commit=False)
		if commit:
			user.save()
		return user



		widgets= {
				'username': forms.TextInput(attrs={'class':'form-control'}),
				'first_name': forms.TextInput(attrs={'class':'form-control'}),
				'last_name': forms.TextInput(attrs={'class':'form-control'}),
				'email': forms.EmailField(attrs={'class':'form-control'}),
			}


