from django.contrib.auth.models import User,Group
from django.test import TestCase, RequestFactory
from .views import RegistroUsuario
from django.test import Client
"""
Todas las pruebas de la aplicacion usuarios
Actualmente soporta las siguientes 4 pruebas:


1. **test_Registrar** 
2. **test_Update** 
3. **test_UsuarioDelete**
4. **test_listar**

**Observaciones:**
1. Correr cada clase Test de forma independiente
"""


c = Client()

"""
PRUEBA UNITARIA PARA PROBAR AGREGAR USUARIO
"""
class Test_registrar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','pablo@gmail.com','admin')
        Group.objects.get_or_create(name='Administrador')
        Group.objects.get_or_create(name='Usuario Normal')

    def registrar_exitoso(self):
        print('PROCESO DE REGISTRAR USUARIO - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el usuario : " ,usuario.has_perm('auth.add_group'))
        response = self.client.post('/usuario/registrar/', data={
                                                                 'username':'peter98', 
                                                                 'first_name':'Pedro',
                                                                 'last_name':'Gonzalez',
                                                                 'email':'pedro@gmail.com', 
                                                                 'groups': '2',
                                                                 'password1': 'ABc123..',
                                                                 'password2': 'ABc123..'
                                                                 })
        usuario = User.objects.get(username='peter98')  # busco el usuario creado
        print(User.objects.all())
        print('Usuario Creado ')
        print('\nUsuario: ' + usuario.username)
        print('\nNombre: ' + usuario.first_name)
        print('\nApellido: ' + usuario.last_name)
        print('\nEmail: ' + usuario.email)
        print('Test de Creacion de Usuario ejecutado exitosamente.\n')
        self.assertEqual(response.status_code, 302)

    def registrar_fallido(self):
        print('PROCESO DE REGISTRAR USUARIO - FALLIDO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el usuario : " ,usuario.has_perm('auth.add_group'))
        response = self.client.post('/usuario/registrar/', data={
                                                                 'username':'peter98', 
                                                                 'first_name':'Pedro',
                                                                 'last_name':'Gonzalez',
                                                                 'email':'pedro@gmail.com', 
                                                                 'password1': 'ABc123..',
                                                                 'password2': 'ABc123..'
                                                                 })
        usuario=User.objects.last()
        print(usuario.username)
        print(response.status_code)
        print('Si el registro fue exitoso retorna 302 y si el registro fue invalido retorna 200 como codigo de estado')
        self.assertEqual(response.status_code, 200)
        print('Si el registro falla como se espera, el ultimo usuario registrado fue pablo')
        self.assertEqual(usuario.username,'pablo')
        print('Test de Registro invalido de Usuario ejecutado exitosamente no se registro usuario nuevo.\n')



"""
PRUEBA UNITARIA PARA PROBAR MODIFICAR USUARIO
"""
class Test_update(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','pablo@gmail.com','admin')
        Group.objects.get_or_create(name='Administrador')
        Group.objects.get_or_create(name='Usuario Normal')
        print(Group.objects.all())
        
    def modificar_exitoso(self):
        print('PROCESO DE MODIFICAR USUARIO - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el usuario : " ,usuario.has_perm('auth.add_group'))
        response = self.client.post('/usuario/registrar/', data={
                                                                 'username':'peter98', 
                                                                 'first_name':'A',
                                                                 'last_name':'A',
                                                                 'email':'', 
                                                                 'groups': '2',
                                                                 'password1': 'ABc123..',
                                                                 'password2': 'ABc123..'
                                                                 })
        usuario = User.objects.get(username='peter98')  # busco el usuario creado
        print('Antes de la edicion')
        print(User.objects.all())
        print('Usuario Creado ')
        print('\nUsuario: ' + usuario.username)
        print('\nNombre: ' + usuario.first_name)
        print('\nApellido: ' + usuario.last_name)
        print('\nEmail: ' + usuario.email)

         ###proceso de Edición de un usuario ya creado
        response = self.client.post('/usuario/editar/2/', data={
                                                                 'username':'peterxxx', 
                                                                 'first_name':'Pedro',
                                                                 'last_name':'Gonzalez',
                                                                 'email':'pedro@gmail.com', 
                                                                 'groups': '1',
                                                                 'password1': 'HJKblkhGKKL4564..',
                                                                 'password2': 'HJKblkhGKKL4564..'
                                                                 })
        usuario = User.objects.get(username='peterxxx')  # busco el usuario creado
        print(User.objects.all())
        print('Despues de la edicion')
        print('Usuario Modificado ')
        print('\nUsuario: ' + usuario.username)
        print('\nNombre: ' + usuario.first_name)
        print('\nApellido: ' + usuario.last_name)
        print('\Email: ' + usuario.email)

        print('Test de Edicion de Usuario ejecutado exitosamente.\n')
        self.assertEqual(response.status_code, 302)

    def modificar_fallido(self):
        print('PROCESO DE MODIFICAR USUARIO - FALLIDO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el usuario : " ,usuario.has_perm('auth.add_group'))
        response = self.client.post('/usuario/registrar/', data={
                                                                 'username':'peter98', 
                                                                 'first_name':'Pedro',
                                                                 'last_name':'Gonzalez',
                                                                 'email':'', 
                                                                 'groups': '2',
                                                                 'password1': 'ABc123..',
                                                                 'password2': 'ABc123..'
                                                                 })
        usuario = User.objects.get(username='peter98')  # busco el usuario creado
        print('Antes de la edicion')
        print(User.objects.all())
        print('Usuario Creado ')
        print('\nUsuario: ' + usuario.username)
        print('\nNombre: ' + usuario.first_name)
        print('\nApellido: ' + usuario.last_name)
        print('\nEmail: ' + usuario.email)

        ###proceso de Edición de un usuario ya creado
        response = self.client.post('/usuario/editar/2/', data={
                                                                 'username':'peterxxx', 
                                                                 'first_name':'A',
                                                                 'last_name':'B',
                                                                 'email':'pedro@gmail.com', 
                                                                 'groups': '1',
                                                                 })
        print(User.objects.all())
        print('Test de Edicion de Usuario invalido ejecutado exitosamente - No se completó correctamente el formulario.\n')
        self.assertEqual(response.status_code, 200)

"""
PRUEBA UNITARIA PARA PROBAR LISTAR USUARIO
"""
class Test_listar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','pablo@gmail.com','admin')
        Group.objects.get_or_create(name='Administrador')
        Group.objects.get_or_create(name='Usuario Normal')
        print(Group.objects.all())
    
    def listar_exitoso(self):
        print('PROCESO DE LISTAR USUARIO - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el usuario : " ,usuario.has_perm('auth.add_group'))
        response = self.client.post('/usuario/registrar/', data={
                                                                 'username':'peter98', 
                                                                 'first_name':'Pedro',
                                                                 'last_name':'Gonzalez',
                                                                 'email':'', 
                                                                 'groups': '2',
                                                                 'password1': 'ABc123..',
                                                                 'password2': 'ABc123..'
                                                                 })
        response= self.client.get('/usuario/listar/')
        self.assertEqual(response.status_code,200)



"""
PRUEBA UNITARIA PARA PROBAR ELIMINAR USUARIO
"""
class Test_eliminar(TestCase):

    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','pablo@gmail.com','admin') #Primer usuario
        user = User.objects.create_superuser('nico','nico@gmail.com','nicopro') #Segundo Usuario
        Group.objects.get_or_create(name='Administrador')
        Group.objects.get_or_create(name='Usuario Normal')
        print(Group.objects.all())
    
    def eliminar_exitoso(self):
        print('PROCESO DE ELIMINAR USUARIO - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el usuario : " ,usuario.has_perm('auth.add_group'))
        response = self.client.post('/usuario/registrar/', data={
                                                                 'username':'peter98', 
                                                                 'first_name':'Pedro',
                                                                 'last_name':'Gonzalez',
                                                                 'email':'', 
                                                                 'groups': '2',
                                                                 'password1': 'ABc123..',
                                                                 'password2': 'ABc123..'
                                                                 })
        usuario = User.objects.get(username='peter98') #Tercer Usuario
        print('Usuario Creado ')
        print(usuario.id)
        print('\tUsuario: ' + usuario.username)
        print('\tNombre: ' + usuario.first_name)
        print('\tApellido: ' + usuario.last_name)
        print('\nEmail: ' + usuario.email)
        print('Antes del borrado')
        print(User.objects.all())
        response= self.client.post('/usuario/eliminar/3/')
        print('Despues del borrado')
        print(User.objects.all())
        print('Test de Borrado de Usuario ejecutado exitosamente.\n')
        self.assertEqual(response.status_code, 302)

    def eliminar_fallido(self):
        print('PROCESO DE ELIMINAR USUARIO - FALLIDO')
        print('PROCESO DE ELIMINAR USUARIO - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el usuario : " ,usuario.has_perm('auth.add_group'))
        response = self.client.post('/usuario/registrar/', data={
                                                                 'username':'peter98', 
                                                                 'first_name':'Pedro',
                                                                 'last_name':'Gonzalez',
                                                                 'email':'', 
                                                                 'groups': '2',
                                                                 'password1': 'ABc123..',
                                                                 'password2': 'ABc123..'
                                                                 })
        usuario = User.objects.get(username='peter98') #Tercer Usuario
        print('Usuario Creado ')
        print(usuario.id)
        print('\tUsuario: ' + usuario.username)
        print('\tNombre: ' + usuario.first_name)
        print('\tApellido: ' + usuario.last_name)
        print('\nEmail: ' + usuario.email)
        print('Antes del borrado')
        print(User.objects.all())
        print('Se suministra un pk inexistente en la url.\n')
        response= self.client.post('/usuarios/eliminar/6/')
        print('Despues del borrado')
        print(User.objects.all())
        print('Test de Borrado de Usuario invalido ejecutado exitosamente.\n')
        self.assertEqual(response.status_code, 404)

     
     
