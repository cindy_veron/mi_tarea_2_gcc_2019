from django.db import models
from apps.us.models import US
from apps.miembros.models import Miembro
from apps.proyecto.models import Proyecto
# Create your models here.

"""
    Modelo Sprint
    
    parametros:
    *Hereda de models.Models
    
    Se especifican los siguientes
    campos requeridos para el modelo:
    estado,
    sprintBacklog,
    sprintTeam,
    duracion,
    fechaInicio,
    fechaFinalizacion,
    proyecto, 
    
    funcion __str__
    *parametro: self
       
    *retorna: nombre del proyecto
"""


class Sprint (models.Model):

    class Meta:
        """
            Clase Meta:
            Establece los parametros de ordenamiento de los objetos de la clase.
        """
        ordering = ["estado","fechaFinalizacion"]

    estado = models.CharField(max_length=18,default="En espera")
    sprintBacklog = models.ManyToManyField(US)
    sprintTeam = models.ManyToManyField(Miembro)
    duracion = models.PositiveIntegerField(default=5)
    fechaInicio = models.DateField(null=True)
    fechaFinalizacion = models.DateField(null=True)
    proyecto = models.ForeignKey(Proyecto,on_delete=models.CASCADE)

    def __str__(self):
        return "Sprint "+str(self.id)+str(self.proyecto.nombre)
