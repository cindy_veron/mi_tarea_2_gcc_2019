from django.conf.urls import url
from .views import verifica_permiso_listar, verifica_permiso_eliminar, verifica_permiso_modificar, verifica_permiso_crear,\
verifica_permiso_detalle, CambiarEstado,verifica_permiso_cambiar_estado

app_name = 'sprint'

urlpatterns = [
    url(r'^crear_sprint$', verifica_permiso_crear, name='crear'),
    url(r'^listar_sprint$', verifica_permiso_listar, name='listar_sprint'),
    url(r'^eliminar_sprint/(?P<tmid>\d+)/$', verifica_permiso_eliminar, name='eliminar_sprint'),
    url(r'^modificar_sprint/(?P<tmid>\d+)/$', verifica_permiso_modificar, name='modificar_sprint'),
    url(r'^detalle_sprint/(?P<tmid>\d+)/$', verifica_permiso_detalle, name='detalle_sprint'),
    # url(r'^cambiar_estado/(?P<tmid>\d+)/$', CambiarEstado.as_view(), name='cambiar_estado'),
    url(r"^(?P<tmid>\d+)/cambiar_estado/", verifica_permiso_cambiar_estado, name="cambiar_estado"),
]
