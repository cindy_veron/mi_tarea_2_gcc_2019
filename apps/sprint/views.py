from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView, UpdateView , DetailView

from apps.miembros.views import has_perm
from .models import Sprint
from .models import Miembro
from apps.proyecto.models import Proyecto
from .forms import FormSprint
from apps.us.models import US
from django.core.mail import EmailMessage
from django.contrib.auth.models import User, Group
from django.shortcuts import get_object_or_404
# Create your views here.
"""
funcion verifica_permisos de listar:
* si el miembro esta autorizado se le 
da acceso ver la lista de los sprints
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_listar(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.consultar_sprint')
    print("h sprint listar")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.consultar_sprint', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return SprintList.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
funcion verifica_permisos de crear:
* si el miembro esta autorizado se le 
da acceso de crear sprints
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_crear(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.consultar_sprint')
    print("h sprint crear")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.consultar_sprint', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    # can_sprint = len(Sprint.objects.filter(estado='pendiente',proyecto=Proyecto.objects.get(id=kwargs['pk'])))
    can_espera = Sprint.objects.filter(estado="En espera", proyecto=Proyecto.objects.get(id=kwargs['pk']))
    can_activo = Sprint.objects.filter(estado="Activo", proyecto=Proyecto.objects.get(id=kwargs['pk']))
    if has_permission and len(can_activo) <= 1 and len(can_espera) <= 1:
        return CrearSprint.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
funcion verifica_permisos de modificar:
* si el miembro esta autorizado se le 
da acceso a modificar los atributos 
del sprint
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_modificar(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.modificar_sprint')
    has_permission|= request.user.has_perm('proyecto.modificar_sprint', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return ModificarSprint.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
funcion verifica_permisos de eliminar:
* si el miembro esta autorizado se le 
da acceso eliminar un sprint
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_eliminar(request, *args, **kwargs):
    has_permission = request.user.has_perm('proyecto.eliminar_sprint')
    print("h sprint eliminar")
    print(has_permission)
    has_permission |= request.user.has_perm('proyecto.eliminar_sprint', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return EliminarSprint.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
funcion verifica_permisos de detalle:
* si el miembro esta autorizado se le 
da acceso ver el contenido del sprint
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_detalle(request,*args,**kwargs):

    # `Return:` HttpResponse de acuerdo al caso.
    proy_id = str(Sprint.objects.get(id=kwargs['tmid']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
        return redirect("proyectos:sprint:detalle_sprint", *args, **kwargs)
    if has_perm(request.user, 'proyecto.ver_kanban', kwargs['pk']):
        return Detalle_sp.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")
"""
    Clase Crear Sprint:
    parametros:
    1. ListView: Renderiza alguna lista de objetos.
    **Retorna:** url a listar_rol

    Se sobreescriben los metodos get_success_url,
    get_context_data para personalizar 
    y mantener los datos en estado correcto
"""

class CrearSprint(CreateView):
   model = Sprint
   fields = ['sprintBacklog','duracion','sprintTeam']
   template_name = 'sprint/crear_sprint.html'

   def post(self, request, *args, **kwargs):
       """
       `Override:` agrega al nuevo Sprint el indicador del proyecto
       al cual pertenece.
       """
       self.object = None
       form_class = self.get_form_class()
       form = self.get_form(form_class)
       form.instance.proyecto = Proyecto.objects.get(id=kwargs['pk'])
       if form.is_valid():
           print("valid")
           ret = self.form_valid(form)
           if verificar_asignacion(request, form.instance):
               asignar_US(request, form.instance)
           return ret
       else:
           print("Invalid")
           return self.form_invalid(form)

   def get_context_data(self, **kwargs):
       """
       `Override:` genera una lista personalizada de User Stories disponibles,
       ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
       """
       context = super(CrearSprint, self).get_context_data(**kwargs)
       context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
       US_list = US.objects.filter(proyecto=context['proyecto'])
       m =Miembro.objects.filter(proyecto=context['proyecto'])
       context['miembros']= m
       sprints_activos = Sprint.objects.filter(proyecto=context['proyecto'])
       sprints_activos = sprints_activos.exclude(estado="Finalizado")

       for i in sprints_activos:
           for j in i.sprintBacklog.all():
               US_list = US_list.exclude(id=j.id)
       context['US_list'] = US_list
       return context

   def get_success_url(self):
       # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
       l = self.kwargs.get('pk')
       return reverse_lazy("proyecto:sprint:listar_sprint", kwargs={'pk': l})
    #####

"""
funcion que verifica la asignacion
de user story 
"""
def verificar_asignacion(request,object):
    # Funcion auxiliar para verificar validez de las asignaciones de US en el sprint,
    # extrayendo la información del `request` y complementando con el objeto resultante
    # `object`.
    for i in object.sprintBacklog.all():
        key = "asignar_"+str(i.id)
        print(key)
        if not key in request.POST:
            return False
        if object.sprintTeam.filter(id=request.POST[key]).count()==0:
            return False

    return True
"""
funcion que asigna el user story
a al miembro encargado de mantener
"""
def asignar_US(request,object):
    # Funcion auxiliar para realizar las asignaciones de US en el sprint, extrayendo
    # la información del `request` y complementando con el objeto resultante `object`.
    try:
        for us in object.sprintBacklog.all():
            key = "asignar_" + str(us.id)
            team = object.sprintTeam.get(id=request.POST[key])
            us.team_member = team
            us.save()
            print(str(us.nombre) + " = "+ str(team.usuario))

            query = User.objects.filter(username=team.usuario)
            if query.count() > 0:
                email = team.usuario.email
                print('email:', email)
                html_content = "Le ha sido asignado el User Story: %s en el proyecto: %s<br> Descripción del US: %s <br>Prioridad: %s<br>"
                message = EmailMessage(subject='Asignación de User Story',
                                       body=html_content % (us.nombre, us.proyecto.nombre, us.descripcion_corta, us.prioridad), to=[email])
                message.content_subtype = 'html'
                message.send()
                print("Notificación enviada a", team.usuario)
            else:
                print("Fallo en la notificación via email")
    except :
        raise Exception("Fallo en la Asignacion")

"""
    Clase Listar Sprint:
    parametros:
    1. ListView: Renderiza alguna lista de objetos.
    **Retorna:** url a lista de Sprint

    Se sobreescriben los metodos get_queryset,
    get_context_data para personalizar 
    y mantener los datos en estado correcto
"""


class SprintList(ListView):
   model = Sprint
   template_name = 'sprint/listar_sprint.html'

   def get_queryset(self):
       self.proyecto = get_object_or_404(Proyecto, id=self.kwargs.get('pk'))
       return Sprint.objects.filter(proyecto=self.proyecto)

   def get_context_data(self, **kwargs):
       context = super(ListView, self).get_context_data(**kwargs)
       context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
       return context


"""
    Clase Eliminar Sprint:
    parametros:
    1. DeleteView: Renderiza alguna lista de objetos.
    **Retorna:** url a listar de Sprint
    
    Se sobreescriben los metodos get_success_url,
    get_context_data y get_object para personalizar 
    y mantener los datos en estado correcto
"""
class EliminarSprint(DeleteView):

   model = Sprint
   template_name = 'sprint/eliminar_sprint.html'

   def get_object(self, queryset=None):
      if queryset is None:
         queryset = self.get_queryset()
      queryset = queryset.filter(id=self.kwargs['tmid'])
      obj = queryset.get()
      return obj

   def get_context_data(self, **kwargs):
       context = super(EliminarSprint, self).get_context_data(**kwargs)
       context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
       return context

   def get_success_url(self):
       l = self.kwargs.get('pk')
       return reverse_lazy('proyecto:sprint:listar_sprint', kwargs={'pk': l})


"""
    Clase Modificar Sprint:
    parametros:
    1. UpdateView: Renderiza alguna lista de objetos.
    **Retorna:** url a lista de Sprint

    Se sobreescriben los metodos get_success_url,
    get_context_data y get_object para personalizar 
    y mantener los datos en estado correcto
"""


class ModificarSprint(UpdateView):

    model = Sprint
    form_class = FormSprint
    template_name = 'sprint/crear_sprint.html'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        context = super(ModificarSprint, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
        context['US_list'] = Sprint.objects.get(id=self.kwargs.get('tmid')).sprintBacklog.all()
        return context

    def get_success_url(self):
        l = self.kwargs.get('pk')
        return reverse_lazy('proyecto:sprint:listar_sprint', kwargs={'pk': l})

"""
    Clase Detalle Sprint:
    parametros:
    1. DetailView: Renderiza alguna lista de objetos.
    **Retorna:** url al detalle del Sprint

    Se sobreescriben los metodos get_success_url,
    get_context_data y get_object para personalizar 
    y mantener los datos en estado correcto
"""
class Detalle_sp(DetailView):
    """
        Visualizador de detalles del Sprint.
        """
    # `Extends:` DetailView; para la visualizacion de los detalles.
    model = Sprint
    template_name = "sprint/detalle_sprint.html"

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del Sprint a mostrar
        """
        if queryset is None:
            queryset = self.get_queryset()
        obj = Sprint.objects.get(id=self.kwargs['tmid'])
        return obj
    def get_context_data(self, **kwargs):
        """
        `Override:` personaliza los datos a visualizar en el menu.
        """
        context = super(DetailView,self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        context['sprint_backlog'] = self.object.sprintBacklog.all()
        if 'filter_by' in self.request.GET:
            print(self.request.GET['filter_by'])
            if self.request.GET['filter_by'] == 'None':
                context['sprint_backlog'] = context['sprint_backlog'].filter(miembro_id=None)
                context['filter_by']= self.request.GET['filter_by']
            else:
                context['filter_by'] = self.request.GET['filter_by']
                context['sprint_backlog']=context['sprint_backlog'].filter(miembro_id=self.request.GET['filter_by'])
        else:
            print("puro")


        context['can_execute'] = Sprint.objects.filter(proyecto_id=self.kwargs["pk"],estado="En ejecución").count() == 0
        context['can_execute_2'] = US.objects.filter(proyecto_id=self.kwargs["pk"],estado_us=2).count() == 0
        return context

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.kwargs.get('pk')
        return reverse_lazy("proyecto:sprint:listar_sprint", kwargs={'pk': l})
"""
funcion verifica_permisos cambiar
estado de un sprint:
* si el miembro esta autorizado se le 
da acceso a cambiar el estado
 de un sprint 
* si no tiene esos permisos se le
 informa que no posee permisos

"""
@login_required()
def verifica_permiso_cambiar_estado(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.ver_proyecto')
    print("h sprint listar")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.ver_proyecto', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        print("tiene permiso y va a la clase cambiar estado")
        return CambiarEstado.as_view()(request, *args, **kwargs)

"""
    Clase Cambiar Estado:
    parametros:
    1. UpdateView: Renderiza alguna lista de objetos.
    **Retorna:** url a la lista de sprint

    Se sobreescriben el metodo get_success_url,
    para personalizar y mantener los datos
     en estado correcto
"""
class CambiarEstado(UpdateView):
    model = Sprint
    fields = ['estado']

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        print("entra en get succes url")
        usid = self.kwargs["tmid"]
        print("sprint ")
        print(self.kwargs["tmid"])
        sprint = Sprint.objects.get(id=usid)
        print(sprint.estado)
        if sprint.estado == 'En espera':
            sprint.estado = "Activo"
            print("ACTIVAR")
            for us in sprint.sprintBacklog.all():
                print("hola")
                if us.estado_us == 0: # si el estado del us esta en espera le pone en ejecucion
                    us.estado_us = 1 # En ejecucion
                    us.save()
        elif sprint.estado == 'Activo':
            sprint.estado = "Cancelado"
            for us in sprint.sprintBacklog.all():
                if us.estado_us == 1: # si el estado del us esta en ejecucion le pone en cancelado
                    us.estado_us = 4 # Cancelado
                    us.save()
        sprint.save()
        return reverse_lazy("proyecto:sprint:listar_sprint", kwargs={'pk': sprint.proyecto.id})



