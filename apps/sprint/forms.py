from django import forms
from .models import Sprint

"""
    Formulario:
    paramteros:
    1. hereda de forms.Modelform

    Class Meta:
    se especifican los campos que seran que
    seran añadidos o modificados:
    * duracion,
    * fechaInicio,
    * fechaFinalizacion,
    * proyecto,
"""
class FormSprint(forms.ModelForm):
    class Meta:
        model = Sprint
        fields =[
            'duracion',
            'fechaInicio',
            'fechaFinalizacion',
            'proyecto',
        ]
        widgets ={
            'duracion': forms.NumberInput(attrs={'class':'form-control'}),
            'fechaInicio':forms.DateTimeInput(attrs= {'class': 'datetime-input'}),
            'fechaFinalizacion': forms.DateTimeInput(attrs= {'class': 'datetime-input'}),
            'proyecto': forms.Select(attrs={'class': 'form-control'}),
        }
