from django.apps import AppConfig


class SprintConfig(AppConfig):
    name = 'apps.sprint'
