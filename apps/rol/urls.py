from django.conf.urls import url, include
from django.contrib.auth.models import Group
from apps.rol.views import RolesList,RolDelete,crear_rol_view,RolModify
urlpatterns = [
    url(r'^listar/',RolesList.as_view(),name='listar'),
    url(r'^eliminar/(?P<pk>\d+)/$',RolDelete.as_view(),name='rol_eliminar'),
    url(r'^crear/', crear_rol_view, name='crear'),
    url(r'^modificar/(?P<pk>\d+)/$', RolModify.as_view(model=Group,), name='rol_modificar'),
]