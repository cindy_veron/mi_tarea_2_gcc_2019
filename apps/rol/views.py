from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group
from django.views.generic import ListView, CreateView , UpdateView , DeleteView
from django.urls import reverse_lazy

from apps.proyecto.models import Proyecto
from apps.rol.forms import GroupForm
from django.shortcuts import render, redirect


"""
    funcion crear rol
    decoradores:
    1. login_required: para verificar que el usuario este
        logueado
    2. permissionRequired: para verificar que el usuario
        posea el permiso para modificar usuarios
    **Retorna:** url a listar_rol si el formulario es valido
                sino mantiene el formulario con los datos
"""
@login_required
@permission_required('auth.add_group')
def crear_rol_view(request):
    if request.method == 'POST':
        form = GroupForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('rol:listar')
    else:
        form = GroupForm()
    return render(request,'rol/roles_form.html', {'form': form})




"""
    1. PermissionRequiredMixin: CBV mixin que verifica que 
        el usuario actual tiene todos los permisos especificados
    2. ListView: Renderiza alguna lista de objetos.
    **Retorna:** url a listar_rol
"""
#===== LISTAR ROLES ====
class RolesList(PermissionRequiredMixin,ListView):
    model = Group
    template_name = 'rol/rol_list.html'
    permission_required = 'auth.change_group'

    def get_queryset(self):
        grupos=Group.objects.filter(~Q(permissions__content_type__model="permiso"))
        print(grupos.count())
        return grupos

"""
    1. PermissionRequiredMixin: CBV mixin que verifica que el 
        usuario actual tiene todos los permisos especificados
    2. DeleteView: Vista para borrar un objeto recolectado con  
        `self.get_object()`, con una respuesta renderizada con una template
    **Retorna:** url a listar_rol
"""
#===== ELIMINAR ROL ====
class RolDelete(PermissionRequiredMixin,DeleteView):
    model = Group
    template_name = 'rol/rol_delete.html'
    permission_required = 'auth.delete_group'
    success_url = reverse_lazy('rol:listar')


"""
    1. PermissionRequiredMixin: CBV mixin que verifica que el 
        usuario actual tiene todos los permisos especificados
    2. UpdateView: Vista para modificar un objeto,con una respuesta 
        renderizada por un template
    **Retorna**: url a listar_rol
"""
#===== MODIFICAR ROL ====
class RolModify(PermissionRequiredMixin,UpdateView):
    model = Group
    form_class = GroupForm
    permission_required = 'auth.change_group'
    template_name = 'rol/rol_modificar.html'
    success_url = reverse_lazy('rol:listar')

