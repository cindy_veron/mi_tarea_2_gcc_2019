from django.contrib.auth.models import Group, Permission
from django import forms
from django.db.models import Q

"""
    Formulario:
    
    hereda de forms.Modelform
    
    permissions: se filtran los permisos para 
    rol de sistema
    
    Class Meta:
    se especifican los campos que seran que
    seran añadidos o modificados:
    *name,
    *permissions
"""

class GroupForm(forms.ModelForm):
    
    permissions = forms.ModelMultipleChoiceField(queryset=Permission.objects.filter(
                                                     Q(codename__icontains='logentry')
                                                   | Q(name='Can add permission')
                                                   | Q(name='Can change permission')
                                                   | Q(name='Can delete permission')
                                                   | Q(name='Can view permission')
                                                   | Q(name='Can add group')
                                                   | Q(name='Can change group')
                                                   | Q(name='Can delete group')
                                                   | Q(name='Can view group')
                                                   | Q(name='Can add user')
                                                   | Q(name='Can change user')
                                                   | Q(name='Can delete user')
                                                   | Q(name='Can view user') 
                                                   | Q(name='Can add content type')
                                                   | Q(name='Can change content')
                                                   | Q(name='Can delete content')
                                                   | Q(name='Can view content')
                                                   | Q(name='Can add session')
                                                   | Q(name='Can change session')
                                                   | Q(codename='add_proyecto')

                                                   ),
                                                   required=True,
                                                   #widget=forms.Select(attrs={'class':'form-control'}))
    )
    
    class Meta:
        model = Group
        fields = [
            'name',
            'permissions',
        ]
        labels = {
            'name':'Nombre del rol',
            'permissions':'Permisos',
        }
