from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User, Group

"""
Todas las pruebas de la aplicacion roles 
actualmente soporta las siguientes 4 pruebas:

1. **test_Registrar** 
2. **test_Modificar** 
3. **test_Eliminar**
4. **test_Listar**

**Observaciones:**
    1. Correr cada clase test de forma independiente
"""

"""
ESTE TEST COMPRUEBA SI SE PUEDE REGISTRAR Y EN QUE CASO NO SE PUEDE REGISTRAR 
UN ROL
"""
class Test_registrar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def registrar_exitoso(self):
        print('PROCESO DE REGISTRAR ROLES - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        response =  self.client.post('/rol/crear/', data={'name': 'Administrador', 'permissions': {'1': 1, '2': 2}})
        var = Group.objects.all().count()
        rol = Group.objects.get(name='Administrador')
        #response = self.client.get('/rol/crear/')
        #print(response.status_code)
        #print(self.client)

        print('Listado de permisos:')
        for x in rol.permissions.all():
            print(x.name)

        print('Si el registro de rol fue válido, que es lo que se espera, se retornará 1')
        print(var)
        self.assertEquals(var, 1)

    def registrar_fallido(self):
        print('PROCESO DE REGISTRAR ROLES - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        response =  self.client.post('/rol/crear/', data={'name': '', 'permissions': {'1': 1, '2': 2}})
        var = Group.objects.all().count()
        print('Si el registro de rol no fue válido, que es lo que se espera, se retornará 0')
        print(var)
        self.assertEquals(var, 0)

"""
ESTE TEST COMPRUEBA SI SE PUEDE LISTAR Y EN QUE CASO NO SE PUEDE LISTAR 
LOS ROLES
"""
class Test_listar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def listar_exitoso(self):
        print('PROCESO DE LISTAR ROLES - EXITOSO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        response1 =  self.client.post('/rol/crear/', data={'name': 'Administrador', 'permissions': {'1': 1, '2': 2}})
        response2 =  self.client.post('/rol/crear/', data={'name': 'Usuario Normal', 'permissions': {'1': 1}})
        var = Group.objects.all().count()
        print("Cantidad de Grupos : ",var)
        response = self.client.get('/rol/listar/')
        var = response.status_code
        print(var)
        self.assertEquals(var, 200)
        print('Se muestra la lista de roles correctamente')
    
    def listar_fallido(self):
        print('PROCESO DE LISTAR ROLES - FALLIDO')
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        response1 =  self.client.post('/rol/crear/', data={'name': 'Administrador', 'permissions': {'1': 1, '2': 2}})
        response2 =  self.client.post('/rol/crear/', data={'name': 'Usuario Normal', 'permissions': {'1': 1}})
        var = Group.objects.all().count()
        print("Cantidad de Grupos : ",var)
        response = self.client.get('/rol/listar') #Acá está el error
        var = response.status_code
        print(var)
        self.assertEquals(var, 301)
        print('No se muestra la lista de roles correctamente')

"""
ESTE TEST COMPRUEBA SI SE PUEDE ELIMINAR Y EN QUE CASO NO SE PUEDE ELIMINAR 
UN ROL
"""
class Test_eliminar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')

    def eliminar_exitoso(self):
        print('PROCESO DE ELIMINAR ROL - EXITOSO')
        self.client.login(username='pablo', password='admin')
        response1 =  self.client.post('/rol/crear/', data={'name': 'Administrador', 'permissions': {'1': 1, '2': 2}})
        print('Roles antes de la eliminacion: ', Group.objects.all())
        response = self.client.post('/rol/eliminar/1/')
        print('Despues de eliminar el rol: ', Group.objects.all())
        self.assertEquals(response.status_code, 302)
        print('El rol se eliminò correctamente ')

    def eliminar_fallido(self):
        print('PROCESO DE ELIMINAR ROL - FALLIDO')
        self.client.login(username='pablo', password='admin')
        response =  self.client.post('/rol/crear/', data={'name': 'Administrador', 'permissions': {'1': 1, '2': 2}})
        print('Roles antes de la eliminacion: ', Group.objects.all())
        response = self.client.post('/rol/eliminar/20/')
        print('Despues de eliminar el rol: ', Group.objects.all())
        self.assertEquals(response.status_code, 404)
        print('El rol no se eliminó')    

   


"""
ESTE TEST COMPRUEBA SI SE PUEDE MODIFICAR Y EN QUE CASO NO SE PUEDE MODIFICAR 
UN ROL
"""
class Test_modificar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')

    def modificar_exitoso(self):
        print('PROCESO DE MODIFICAR ROL - EXITOSO')
        self.client.login(username='pablo', password='admin')
        response =  self.client.post('/rol/crear/', data={'name': 'Administrador', 'permissions': {'1': 1, '2': 2, '3': 3}})
        rol = Group.objects.get(pk=1)
        print('Rol antes de la modificación:\nId del rol creado:', rol.id, '\tNombre: ', rol.name, '\nPermisos: ')
        for x in rol.permissions.all():
            print(x.name)
        self.client.post('/rol/modificar/1/', data={'name': 'Usuario Normal', 'permissions': {'1': 1}})
        rol = Group.objects.get(pk=1)
        print('Rol despues de la modificación:\nId del rol creado:', rol.id, '\tNombre: ', rol.name, '\nPermisos: ')
        for x in rol.permissions.all():
            print(x.name)
        self.assertEquals(rol.name, 'Usuario Normal')
        print('El rol se modificó correctamente')

    def modificar_fallido(self):
        print('PROCESO DE MODIFICAR ROL - FALLIDO')
        self.client.login(username='pablo', password='admin')
        response =  self.client.post('/rol/crear/', data={'name': 'Administrador', 'permissions': {'1': 1, '2': 2, '3': 3}})
        rol = Group.objects.get(pk=1)
        print('Rol antes de la modificación:\nId del rol creado:', rol.id, '\tNombre: ', rol.name, '\nPermisos: ')
        for x in rol.permissions.all():
            print(x.name)
        self.client.post('/rol/modificar/1/', data={'name': '', 'permissions': {'1': 1}})
        rol = Group.objects.get(pk=1)
        print('Rol despues de la modificación:\nId del rol creado:', rol.id, '\tNombre: ', rol.name, '\nPermisos: ')
        for x in rol.permissions.all():
            print(x.name)
        self.assertEquals(rol.name, 'Administrador')
        print('El rol no se modificó como se esperaba')
