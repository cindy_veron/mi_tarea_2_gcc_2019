from django.urls import path,include
from django.conf.urls import url,include
from apps.us.views import crearUS, USList, modificarUS, eliminarUS, detalleUS
#from apps.us.views import verifica_permiso_modificar, verifica_permiso_eliminar, verifica_permiso_crear, verifica_permiso_detalle


app_name = 'us'

urlpatterns = [
    url(r'^crear_us$', crearUS.as_view(), name='crear'),
    url(r'^listar_us$',USList.as_view(), name='listar_us'),
    url(r'^modificar_us/(?P<tmid>\d+)/$', modificarUS.as_view(), name='modificar_us'),
    url(r'^eliminar_us/(?P<tmid>\d+)/$', eliminarUS.as_view(), name='eliminar_us'),
    url(r'^detalle_us/(?P<tmid>\d+)/$', detalleUS.as_view(), name='detalle_us'),
    url(r'^(?P<tmid>\d+)/nota/', include('apps.nota.urls', namespace='nota')),
    #url(r'^detalle_us/(?P<usid>\d+)/historial/$', HistorialUSListView.as_view(), name='historial'),
]
"""
urlpatterns = [
    url(r'^crear_us$', verifica_permiso_crear, name='crear'),
    url(r'^listar_us$', USList, name='listar_us'),
    url(r'^modificar_us/(?P<tmid>\d+)/$', verifica_permiso_modificar, name='modificar_us'),
    url(r'^eliminar_us/(?P<tmid>\d+)/$', verifica_permiso_eliminar, name='eliminar_us'),
    url(r'^detalle_us/(?P<tmid>\d+)/$', verifica_permiso_detalle, name='detalle_us'),
    #url(r'^detalle_us/(?P<usid>\d+)/historial/$', HistorialUSListView.as_view(), name='historial'),
]
"""