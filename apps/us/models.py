from django.core.validators import MaxValueValidator
from django.db import models
from apps.proyecto.models import Proyecto
from apps.tipous.models import TipoUS, Actividad
from apps.miembros.models import Miembro

estados_kanban = ((0,'To do'),(1,'Doing'),(2,'Done'))
estados_us = ((0,'En espera'), (1,'En ejecucion'), (2, 'Control de calidad'), (3, 'Finalizado'), (4, 'Cancelado'))

class US(models.Model):
    """
    Clase UserStory:
    Registra un nombre, una descripcion corta,
    una descriocion larga, una prioridad,
    una fase, un estado, una fecha de modificacion,
    un valor de negocio y tecnico, un archivo adjunto,
    un tiempo estimado y dedicado para el user story y
    lo asocia a un proyecto, a un tipo de user story
    y desarrolador.
    """
    nombre = models.CharField(max_length=100, unique=True)
    descripcion_corta = models.CharField(max_length=100)
    descripcion_larga = models.TextField()
    prioridad = models.IntegerField(choices=((i, i) for i in range(1, 11)), default=1)
    prioridad_final = models.IntegerField(default=0)
    actividad_actual = models.ForeignKey(Actividad, on_delete=models.SET_NULL, null=True)
    estado_kanban = models.IntegerField(choices=estados_kanban, default=0)
    estado_us = models.IntegerField(choices=estados_us, default=0)
    tipo_us = models.ForeignKey(TipoUS, on_delete=models.SET_NULL, null=True)
    proyecto = models.ForeignKey(Proyecto, null=True,blank=True,on_delete=models.CASCADE)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    valor_negocio = models.PositiveIntegerField(validators=[MaxValueValidator(10)])
    valor_tecnico = models.PositiveIntegerField(validators=[MaxValueValidator(10)])
    archivo_adjunto = models.FileField(default=None, blank=True)
    # historial =
    tiempo_estimado = models.IntegerField(default=3)
    tiempo_dedicado = models.IntegerField(default=0)
    team_member = models.ForeignKey(Miembro, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        """
        Meta-Clase:
        Establece los parametros de ordenamiento
         de los objetos de la clase.
        """
        ordering = ['-prioridad_final', "nombre"]

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        old = US.objects.filter(id=self.id)
        #si hay objeto en el US
        #if old.count() > 0:
        #Calcula la prioridad final mediante la formula dada en la ERS
        self.prioridad_final = (2*self.valor_negocio+self.prioridad+2*self.valor_tecnico)/5
        #Si el tiempo dedicado excede al tiempo estimado se prioriza el US dandole un valor grande
        #falta mas condiciones
        if self.tiempo_dedicado > self.tiempo_estimado:
            self.prioridad_final = 12

        super().save(force_insert=force_insert,
                     force_update=force_update,
                     using=using,
                     update_fields=update_fields)
    """
    **__str__** retorna una cadena que representa el nombre del user story del proyecto en cuestion
    """

    def __str__(self):
        return self.nombre


