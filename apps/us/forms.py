from django import forms
from apps.us.models import US
from apps.tipous.models import TipoUS

"""
    Formulario:
    parametros:
    1. hereda de forms.Modelform

    Class Meta:
    se especifican los campos que seran que
    seran añadidos o modificados:
    *nombre,
    *descripcion_corta,
    *descripcion_larga,
    *prioridad,
    *tipo_us,
    *valor_negocio,
    *valor_tecnico,
    *archivo_adjunto,
    *tiempo_estimado,
"""


class FormUserStory(forms.ModelForm):
    class Meta:
        model = US
        fields =[
            'nombre',
            'descripcion_corta',
            'descripcion_larga',
            'prioridad',
            'tipo_us',
            'valor_negocio',
            'valor_tecnico',
            'archivo_adjunto',
            'tiempo_estimado',
        ]
        labels={
            'nombre':'Nombre de US',
            'descripcion_corta':'Descripcion Larga',
            'descripcion_larga':'Descripcion Corta',
            'prioridad': 'Prioridad',
            'tipo_us':'Tipo De US',
            'valor_negocio':'Valor de Negocio',
            'valor_tecnico':'Valor Tecnico',
            'archivo_adjunto':'Archivo Adjunto',
            'tiempo_estimado':'Tiempo Estimado',
        }
        widgets ={
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'descripcion_corta':forms.TextInput(attrs={'class': 'form-control'}),
            'descripcion_larga': forms.Textarea(attrs={'class': 'form-control'}),
            'prioridad': forms.NumberInput(attrs={'class': 'form-control'}),
            'tipo_us': forms.Select(attrs={'class':'form-control'}),
            'valor_negocio': forms.NumberInput(attrs={'class': 'form-control'}),
            'valor_tecnico': forms.NumberInput(attrs={'class': 'form-control'}),
            'archivo_adjunto':forms.FileInput(),
            'tiempo_estimado': forms.NumberInput(attrs={'class': 'form-control'}),
        }
