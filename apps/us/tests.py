from django.test import TestCase
from django.test import Client
from apps.tipous.models import TipoUS, Actividad
from django.contrib.auth.models import User
from apps.us.models import US
from django.contrib.auth.models import User,Group,Permission
from apps.proyecto.models import Proyecto

"""
    Esta clase se encarga de realizar prueba unitaria para crear un User Story
    En caso exitoso retorna 302
    En caso fallido retorna 200
    Primero se crea un usuario con permisos de administrador
    Luego se crea un proyecto
    por ultimo se crea user story perteneciente a dicho proyecto
"""
class Test_registrar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def registrar_exitoso(self):
        print('PROCESO DE MODIFICAR USER STORY - EXITOSO')
         #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        print("\nCreación de tipo US") 
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })

     #######################PROCESO DE REGISTRAR US#########################
                                       
        print("\nCreación de US")                                                       
        response =  self.client.post('/proyecto/1/us/crear_us', data={  'nombre':'US1',
                                                                        'descripcion_corta': 'URS1',
                                                                        'descripcion_larga': 'USER STORY 1',
                                                                        'tipo_us': 1,
                                                                        'prioridad': 1,
                                                                        'valor_negocio': 1,
                                                                        'valor_tecnico': 1,
                                                                        'archivo_adjunto': '(binary)',
                                                                        'tiempo_estimado': 1,
                                                                })
        var = response.status_code
        cantidad = US.objects.all()
        print("Proceso correcto...")
        print(cantidad)
        self.assertEquals(var, 302)                                                         

    def registrar_fallido(self):
        print('PROCESO DE MODIFICAR USER STORY - EXITOSO')
         #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        print("\nCreación de tipo US") 
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })

     #######################PROCESO DE REGISTRAR US#########################
                                       
        print("\nCreación de US")                                                       
        response =  self.client.post('/proyecto/1/us/crear_us', data={  'nombre':'US1',
                                                                        'descripcion_corta': 'URS1',
                                                                        'descripcion_larga': 'USER STORY 1',
                                                                       
                                                                })
        var = response.status_code
        cantidad = US.objects.all()
        print("Proceso correcto...")
        print(cantidad)
        self.assertEquals(var, 200) 








class Test_modificar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def modificar_exitoso(self):
        print('PROCESO DE MODIFICAR USER STORY - EXITOSO')
         #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        print("\nCreación de tipo US") 
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })

     #######################PROCESO DE REGISTRAR US#########################
                                       
        print("\nCreación de US")                                                       
        response =  self.client.post('/proyecto/1/us/crear_us', data={  'nombre':'US1',
                                                                        'descripcion_corta': 'URS1',
                                                                        'descripcion_larga': 'USER STORY 1',
                                                                        'tipo_us': 1,
                                                                        'prioridad': 1,
                                                                        'valor_negocio': 1,
                                                                        'valor_tecnico': 1,
                                                                        'archivo_adjunto': '(binary)',
                                                                        'tiempo_estimado': 1,
                                                                })
        cantidad = US.objects.all()
        print("Creado...")
        print(cantidad)
        print("\Modificación de US creado anteriormente")                                                       
        response =  self.client.post('/proyecto/1/us/modificar_us/1/', data={'nombre':'US2',
                                                                        'descripcion_corta': 'URS2',
                                                                        'descripcion_larga': 'USER STORY 2',
                                                                        'tipo_us': 1,
                                                                        'prioridad': 1,
                                                                        'valor_negocio': 1,
                                                                        'valor_tecnico': 1,
                                                                        'archivo_adjunto': '(binary)',
                                                                        'tiempo_estimado': 1,
                                                                })
                                                   
        var = response.status_code
        cantidad = US.objects.all()
        print("Proceso correcto...")
        print(cantidad)
        self.assertEquals(var, 302)   

     
    def modificar_fallido(self):
        print('PROCESO DE MODIFICAR USER STORY - FALLIDO')
         #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        print("\nCreación de tipo US") 
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })

     #######################PROCESO DE REGISTRAR US#########################
                                       
        print("\nCreación de US")                                                       
        response =  self.client.post('/proyecto/1/us/crear_us', data={  'nombre':'US1',
                                                                        'descripcion_corta': 'URS1',
                                                                        'descripcion_larga': 'USER STORY 1',
                                                                        'tipo_us': 1,
                                                                        'prioridad': 1,
                                                                        'valor_negocio': 1,
                                                                        'valor_tecnico': 1,
                                                                        'archivo_adjunto': '(binary)',
                                                                        'tiempo_estimado': 1,
                                                                })
        cantidad = US.objects.all()
        print("Creado...")
        print(cantidad)
        print("\Modificación de US creado anteriormente")                                                       
        response =  self.client.post('/proyecto/1/us/modificar_us/', data={'nombre':'US2',
                                                                        'descripcion_corta': 'URS2',
                                                                        'descripcion_larga': 'USER STORY 2',
                                                                        'tipo_us': 1,
                                                                        'prioridad': 1,
                                                                        'valor_negocio': 1,
                                                                        'valor_tecnico': 1,
                                                                        'archivo_adjunto': '(binary)',
                                                                        'tiempo_estimado': 1,
                                                                })
                                                   
        var = response.status_code
        cantidad = US.objects.all()
        print("Proceso incorrecto...")
        print(cantidad)
        self.assertEquals(var, 404)   



class Test_eliminar(TestCase):
    def setUp(self):
        print("\n\n\n\nCrea un superusuario")
        user = User.objects.create_superuser('pablo','','admin')
        
    def eliminar_exitoso(self):
        print('PROCESO DE ELIMINAR USER STORY - EXITOSO')
         #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        print("\nCreación de tipo US") 
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })

     #######################PROCESO DE REGISTRAR US#########################
                                       
        print("\nCreación de US")                                                       
        response =  self.client.post('/proyecto/1/us/crear_us', data={  'nombre':'US1',
                                                                        'descripcion_corta': 'URS1',
                                                                        'descripcion_larga': 'USER STORY 1',
                                                                        'tipo_us': 1,
                                                                        'prioridad': 1,
                                                                        'valor_negocio': 1,
                                                                        'valor_tecnico': 1,
                                                                        'archivo_adjunto': '(binary)',
                                                                        'tiempo_estimado': 1,
                                                                })
        cantidad = US.objects.all()
        print("Creado...")
        print(cantidad)

    #######################PROCESO DE ELIMINAR US#########################
        print("\Eliminación de US creado anteriormente")                                                       
        response =  self.client.post('/proyecto/1/us/eliminar_us/1/')                   
        var = response.status_code
        cantidad = US.objects.all()
        print("Proceso correcto...")
        print(cantidad)
        self.assertEquals(var, 302)   

    def eliminar_fallido(self):
        print('PROCESO DE ELIMINAR USER STORY - FALLIDO')
         #######################PROCESO DE CREAR PROYECTO#########################
        self.client.login(username='pablo', password='admin')
        usuario = User.objects.get(username='pablo')
        User.objects.create_user(username='ramon', password='admin')
        usuario2=User.objects.get(id=3)
        print('El usuario existente es: ', usuario)
        print("tiene permiso para crear el grupo : " ,usuario.has_perm('auth.add_group'))
        #Listado de permisos
        perm_scrum=[
                'cancelar_proyecto',
                'suspender_proyecto',
                'reanudar_proyecto',
                'ver_proyecto',
                'modificar_proyecto',
                'eliminar_proyecto',
                'agregar_miembro_proyecto',
                'eliminar_miembro_proyecto',
                'modificar_miembro_proyecto',
                'verlista_miembro_proyecto',
                'agregar_miembro_sprint',
                'eliminar_miembro_sprint',
                'modificar_miembro_sprint',
                'verlista_miembro_sprint',
                'crear_rol_proyecto',
                'modificar_rol_proyecto',
                'eliminar_rol_proyecto',
                'ver_rol_proyecto',
                'crear_tus',
                'modificar_tus',
                'eliminar_tus',
                'ver_tus',
                'importar_tus',
                'ver_productBacklog',
                'ver_sprintBacklog',
                'ver_kanban',
                'crear_us',
                'modificar_us',
                'eliminar_us',
                'ver_us',
                'detalle_us',
                'crear_sprint',
                'modificar_sprint',
                'consultar_sprint',
                'eliminar_sprint',
        ]
        #Crear rol Scrum Master
        Group.objects.get_or_create(name='Scrum Master')

        #Asignar permisos al rol Scrum Master 
        for i in perm_scrum:
            try:
                grupo = Group.objects.get(name="Scrum Master")
                permiso = Permission.objects.get(Q(content_type__model="permiso") & Q(codename=i))
                grupo.permissions.add(permiso)
            except:
                print("Fail "+ str(i))
        
        #Asignar rol al usuario2 
        grupo = Group.objects.get(name="Scrum Master")   
        grupo.user_set.add(usuario2)

        print('El usuario scrum es: ', usuario2)
        print("\nCreación de PROYECTO")
        response =  self.client.post('/proyecto/registrar/', data={'codigo':1,
                                                                   'nombre':'proyecto1',
                                                                   'descripcion':'descripcion proyecto 1',
                                                                   'fechaInicio':'10/05/2019 23:07:33',
                                                                   'fechaFin':'10/05/2019 23:07:34',
                                                                   'scrum':3,
                                                                })
        cantidad = Proyecto.objects.all()
     #######################PROCESO DE REGISTRAR TIPO US#########################
        print("\nCreación de tipo US") 
        response =  self.client.post('/proyecto/1/tipous/registrar/', data={'nombre':'TUS',
                                                                            'descripcion':'TIPO DE USER STORY',
                                                                            'actividad_set-TOTAL_FORMS':2,
                                                                            'actividad_set-INITIAL_FORMS':0,
                                                                            'actividad_set-MIN_NUM_FORMS':0,
                                                                            'actividad_set-MAX_NUM_FORMS':1000,
                                                                            'actividad_set-0-id':'',
                                                                            'actividad_set-0-tipoUS':'',
                                                                            'actividad_set-0-nombre':'ACTIVIDAD',
                                                                            'actividad_set-0-DELETE':'',
                                                                            'actividad_set-1-id':'', 
                                                                            'actividad_set-1-tipoUS':'', 
                                                                            'actividad_set-1-nombre':'', 
                                                                })

     #######################PROCESO DE REGISTRAR US#########################
                                       
        print("\nCreación de US")                                                       
        response =  self.client.post('/proyecto/1/us/crear_us', data={  'nombre':'US1',
                                                                        'descripcion_corta': 'URS1',
                                                                        'descripcion_larga': 'USER STORY 1',
                                                                        'tipo_us': 1,
                                                                        'prioridad': 1,
                                                                        'valor_negocio': 1,
                                                                        'valor_tecnico': 1,
                                                                        'archivo_adjunto': '(binary)',
                                                                        'tiempo_estimado': 1,
                                                                })
        cantidad = US.objects.all()
        print("Creado...")
        print(cantidad)

    #######################PROCESO DE ELIMINAR US#########################
        print("\Eliminación fallida de US creado anteriormente")                                                       
        response =  self.client.post('/proyecto/1/us/eliminar_us/')                   
        var = response.status_code
        cantidad = US.objects.all()
        print("Proceso incorrecto...")
        print(cantidad)
        self.assertEquals(var, 404)  