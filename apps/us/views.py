from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from apps.proyecto.models import Proyecto
from apps.tipous.models import Actividad
from apps.us.models import US
from apps.us.forms import FormUserStory
from apps.us.models import TipoUS
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, UpdateView, DeleteView, DetailView
from django.views.generic.edit import CreateView
from django.contrib import messages

"""
@login_required()
def verifica_permiso_listar(request,*args,**kwargs):
    ##has_permission = request.user.has_perm('proyecto.detalle_us')
    ##print("h detalle us")
  ##  print(has_permission)
   ## has_permission|= request.user.has_perm('proyecto.detalle_us', Proyecto.objects.get(id=kwargs['pk']))
   ## print(has_permission)
    ##if has_permission:
    return detalleUS.as_view()(request, *args, **kwargs)
   ## else:
     ##   return HttpResponse("<html><p>No posee permisos</p></html>")


@login_required()
def verifica_permiso_detalle(request,*args,**kwargs):
   ## has_permission = request.user.has_perm('proyecto.detalle_us')
    print("h detalle us")
   ## print(has_permission)
   ## has_permission|= request.user.has_perm('proyecto.detalle_us', Proyecto.objects.get(id=kwargs['pk']))
   ## print(has_permission)
   ## if has_permission:
    return detalleUS.as_view()(request, *args, **kwargs)
   ## else:
    ##    return HttpResponse("<html><p>No posee permisos</p></html>")


@login_required()
def verifica_permiso_crear(request,*args,**kwargs):
   ## has_permission = request.user.has_perm('proyecto.crear_us')
    print("h crear us")
   ## print(has_permission)
    ##has_permission|= request.user.has_perm('proyecto.crear_us', Proyecto.objects.get(id=kwargs['pk']))
   ## print(has_permission)
   ## if has_permission:
    return crearUS.as_view()(request, *args, **kwargs)
   ## else:
     ##   return HttpResponse("<html><p>No posee permisos</p></html>")


@login_required()
def verifica_permiso_modificar(request,*args,**kwargs):
    has_permission = request.user.has_perm('proyecto.modificar_us')
    print("h modificar us")
    print(has_permission)
    has_permission|= request.user.has_perm('proyecto.modificar_us', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return modificarUS.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


@login_required()
def verifica_permiso_eliminar(request, *args, **kwargs):
    has_permission = request.user.has_perm('proyecto.eliminar_us')
    print("h eliminar us")
    print(has_permission)
    has_permission |= request.user.has_perm('proyecto.eliminar_us', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return eliminarUS.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""

"""
    Creación de User Story.
    **Hereda de:**

    1. CreateView: Vista para la creacion de 
    una nueva instancia de un objeto con una 
    respuesta renderizada por un template

    **Retorna:** url a listarUS

"""


class crearUS(CreateView):
   model = US
   form_class = FormUserStory
   template_name = 'us/crear_us.html'
   def get_context_data(self, **kwargs):
      """
      `Override:` genera una lista personalizada
      de miembros de team,  ademas agrega datos del
      proyecto. A fin de visualizar dichos datos en el menu.
      """
      context = super(CreateView, self).get_context_data(**kwargs)
      context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
      pro = Proyecto.objects.get(pk=self.kwargs.get('pk'))
      context['tus_list'] = TipoUS.objects.filter(proyecto=pro)
      return context

   def form_valid(self, form):
       us =form.save(commit= False)
       proyecto= Proyecto.objects.get(pk = self.kwargs.get('pk'))
       prio=((2*us.valor_tecnico+2*us.valor_negocio)/5)
       us.prioridad_final=prio
       us.proyecto = proyecto
       print("us.tipous")
       print(us.tipo_us.id)
       actividades=Actividad.objects.filter(tipoUS_id=us.tipo_us.id)
       print('actividades')
       print(actividades[0])
       us.actividad_actual=actividades[0]
       return super().form_valid(form)

   def get_success_url(self):
       # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
       l = self.object.proyecto.id
       return reverse_lazy("proyecto:us:listar_us", kwargs={'pk': l})

   class Meta:
       ordering = ['prioridad_final']


"""
    Menu de listado de user stories del proyecto.
    **Hereda de:**

    1. ListView: Renderiza alguna lista de objetos,
    seteado por `self.model` or `self.queryset`.
    `self.queryset` puede ser cualquier iterable 
    de items no solo un queryset

    **Retorna:** url a listarUS
"""
    # `Extends:` ListView; para la visualizacion de los user stories.


class USList(ListView):
   model = US
   template_name = 'us/listar_us.html'
   permission_required = 'auth.change_user'

   def get_queryset(self):
       """
       `Override:` personaliza la lista de user stories
       a fin de proyectar solo los  pertenecientes al proyecto.
       """
       self.proyecto = get_object_or_404(Proyecto, id=self.kwargs.get('pk'))
       return US.objects.filter(proyecto=self.proyecto)

   def get_context_data(self, **kwargs):
       """
       `Override:` agrega mas informacion para complementar los datos del menu.
       """
       context = super(ListView, self).get_context_data(**kwargs)
       context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
       return context

# === modificarUS ===


class modificarUS(UpdateView):
    """
    Menú para la modificación de user story
    **Hereda de:**

    1. UpdateView: Vista para modificar un objeto,con una
     respuesta renderizada por un template

    **Retorna**: url a listarUS
     """
    # `Extends:` EditView; para la visualizacion del menu de modificación.
    model = US
    form_class = FormUserStory
    template_name = 'us/modificar_us.html'
    #success_url = reverse_lazy('us:listar_us')

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identificación del user
        story a ser modificado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de
        miembros de team, ademas agrega datos del proyecto.
         A fin de visualizar dichos datos en el menu.
        """
        context = super(modificarUS, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
        pro = Proyecto.objects.get(pk=self.kwargs.get('pk'))
        context['tus_list'] = TipoUS.objects.filter(proyecto=pro)
        return context

    def form_valid(self, form):
        # if form.is_valid():
        #     print("HELLO")
        # else:
        #     print("ERROR")
        #     print(form.errors.as_data())
            # messages.error(self.request,"Error")

        us = form.save(commit=False)
        proyecto = Proyecto.objects.get(pk=self.kwargs.get('pk'))
        prio = ((2 * us.valor_tecnico + 2 * us.valor_negocio+ us.prioridad) / 5)
        us.prioridad_final = prio
        us.proyecto = proyecto
        print("us.tipous")
        print(us.tipo_us.id)
        actividades = Actividad.objects.filter(tipoUS_id=us.tipo_us.id)
        print('actividades')
        print(actividades[0])
        us.actividad_actual = actividades[0]
        return super().form_valid(form)

    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.kwargs.get('pk')
        # print(l)
        return reverse_lazy('proyecto:us:listar_us', kwargs={'pk': l})


class eliminarUS(DeleteView):
   """
   Solicitud de confirmacion para la eliminación de un user
   story del proyecto.

   **Hereda de:**

   1. DeleteView: Vista para borrar un objeto recolectado con
    `self.get_object()`, con una respuesta renderizada con una template

   **Retorna:** url a listarUS

   """
   # `Extends:` DeleteView; para visualizacion de la conformacion de expulsión.

   model = US
   template_name = 'us/eliminar_us.html'
   #success_url = reverse_lazy('us:listar_us')
   def get_object(self, queryset=None):
      """
      `Override:` Personaliza la identifiación del user story a ser eliminado.
      """
      if queryset is None:
         queryset = self.get_queryset()
      queryset = queryset.filter(id=self.kwargs['tmid'])
      obj = queryset.get()
      return obj

   def get_context_data(self, **kwargs):
       """
       `Override:` genera una lista personalizada de miembros de team,
       ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
       """
       context = super(eliminarUS, self).get_context_data(**kwargs)
       context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
       return context

   def get_success_url(self):
       # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
       l = self.kwargs.get('pk')
       #print(l)
       return reverse_lazy('proyecto:us:listar_us', kwargs={'pk': l})


class detalleUS(DetailView):
    """
    **Hereda de:**

    1.DetailView: por defecto esta es una instancia
        de modelo para `self.queryset` pero
        la vista soporta mostrar cualquier
        objeto si se sobreescribe `self.get_object()

    **Retorna:** url a listarUS
    """
    model = US
    template_name = 'us/detalle_us.html'
    #success_url = reverse_lazy('us:listar_us')

    def get_object(self, queryset=None):
        """
        `Override:` Personaliza la identifiación del user story a ser visualizado.
        """
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['tmid'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        """
        `Override:` genera una lista personalizada de miembros de team,
        ademas agrega datos del proyecto. A fin de visualizar dichos datos en el menu.
        """
        context = super(detalleUS, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs.get('pk'))
        return context



    def get_success_url(self):
        # `Override:` personaliza la url de redireccion para casos de exito en la operacion.
        l = self.kwargs.get('pk')
        print(l)
        return reverse_lazy('proyecto:us:listar_us', kwargs={'pk': l})

def listar_us_finalizados(request, *args, **kwargs):
    """
    En caso de comprobar que el usuario "logueado" posea los permisos necesarios,
    despliega un menu con un listado de los user stories finalizados del proyecto, o un
    mensaje de alerta si no los posee.
    """
    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyecto.ver_us')
    has_permission |= request.user.has_perm('proyecto.ver_us', Proyecto.objects.get(id=kwargs['pk']))
    has_permission = request.user.has_perm('proyecto.ver_us_finalizado')
    has_permission |= request.user.has_perm('proyecto.ver_us_finalizado', Proyecto.objects.get(id=kwargs['pk']))
    if has_permission:
        return listarUSfinalizados.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


class listarUSfinalizados(ListView):
    """
    Menu de listado de user stories finalizados del proyecto.
    **Hereda de:**

    1. ListView: Renderiza alguna lista de objetos,seteado por
    `self.model` or `self.queryset`.`self.queryset` puede ser cualquier
    iterable de items no solo un queryset

    **Retorna:** url a proyecto
     """
    # `Extends:` ListView; para la visualizacion de los user stories.
    model = US
    template_name = 'us/us_finalizados.html'
    paginate_by = 10

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return US.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        us_list = US.objects.filter(proyecto=context['proyecto'], estado=3)
        context['us_list'] = us_list
        return context
