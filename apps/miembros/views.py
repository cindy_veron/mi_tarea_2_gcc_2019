from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import redirect
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User, Group
from django.views.generic import CreateView, ListView, DeleteView, UpdateView
from apps.miembros.models import Miembro
from apps.proyecto.models import Proyecto
from apps.us.models import US
from django.contrib.auth.mixins import PermissionRequiredMixin


"""
funcion verifica_permisos de agregar:
* si el miembro esta autorizado se le 
da acceso agregar miembros del proyecto
* si no tiene esos permisos se le
 informa que no posee permisos

"""


@login_required()
def verifica_permiso_agregar(request,*args,**kwargs):
    if has_perm(request.user, 'proyecto.agregar_miembro_proyecto', kwargs['pk']):
        print("tiene permiso agregar miembro")
        return AgregarMiembro.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><h1><p>No posee permisos</p></h1></html>")

""""
funcion verifica_permisos de listar:
* si el miembro esta autorizado se le da 
acceso para ver la lista de miembros del Proyecto
* si no tiene esos permisos se le 
informa que no posee permisos
"""


@login_required()
def verifica_permiso_listar(request, *args, **kwargs):
    if has_perm(request.user, 'proyecto.verlista_miembro_proyecto', kwargs['pk']):
        print("tiene permiso ver lista miembro")
        return ListarMiembro.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><h1><p>No posee permisos</p></h1></html>")


"""
funcion verifica_permisos de modificar:
* si el miembro esta autorizado se le da 
acceso modificar miembros del proyecto
* si no tiene esos permisos se le
 informa que no posee permisos
"""


@login_required()
def verifica_permiso_modificar(request, *args, **kwargs):
    proy_id = str(Miembro.objects.get(id=kwargs['id_']).proyecto_id)
    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
        return redirect("miembro:modificar", *args, **kwargs)
    if has_perm(request.user, 'proyecto.modificar_miembro_proyecto', kwargs['pk']):
        print("tiene permiso modificar miembro")
        return ModificarMiembro.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><h1><p>No posee permisos</p></h1></html>")

"""
funcion verifica_permisos de eliminar:
* si el miembro esta autorizado se le da
 acceso eliminar miembros del proyecto
* si no tiene esos permisos se le informa 
que no posee permisos
"""


@login_required()
def verifica_permiso_eliminar(request, *args, **kwargs):
    proy_id = str(Miembro.objects.get(id=kwargs['ide']).proyecto_id)

    if proy_id != kwargs['pk']:
        kwargs['pk'] = proy_id
        return redirect("proyecto:miembro:eliminar", *args, **kwargs)
    if has_perm(request.user, 'proyecto.eliminar_miembro_proyecto', kwargs['pk']):
        print("tiene permiso eliminar miembro")
        return EliminarMiembro.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><h1><p>No posee permisos</p></h1></html>")


"""
Clase Agregar Miembro el miembro que esta 
autorizado puede agregar miembros en el cual 
especifica el usuario, el rol que posee 
en el proyecto, las horas que trabaja

ademas se modifican los metodos post, 
get_context_data y get_success_url
para personalizar y mantener los datos 
en estado correcto

"""


class AgregarMiembro(CreateView):
    model = Miembro
    fields = ['usuario', 'rolProyecto', 'horasDeTrabajo']
    template_name = "miembro/Añandir_miembro.html"


    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.instance.proyecto = Proyecto.objects.get(id=kwargs['pk'])
        if form.is_valid():
            return self.form_valid(form)
        else:
            print(False)
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        #sobreescibe el contexto
        context = super(CreateView, self).get_context_data(**kwargs)
        #crea el contexto proyecto con la id de proyecto actual
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        #Crea la lista de Miembros que pertenece al proyecto
        miembrosLista = Miembro.objects.filter(proyecto=context['proyecto'])
        #Crea una lista de usuarios del sistema que estan activos y no son superusuarios
        usuariosLista = User.objects.all().filter(is_active=True,is_superuser=False)
        # de la lista de usuarios excluye a los usuarios que ya son miembros del proyecto
        for usuario in miembrosLista:
            usuariosLista = usuariosLista.exclude(id=usuario.usuario.id)
        # Excluye el AnonymousUser
        usuariosLista = usuariosLista.exclude(username="AnonymousUser")
        # Excluye los usuarios Administradores
        for admin in User.objects.filter(groups__name="Administrador"):
            usuariosLista = usuariosLista.exclude(id=admin.id)
        # Crea el contexto user_list con la lista de usuarios
        context['user_list'] = usuariosLista
        # Crea el contexto rol_list excluyendo a los roles que no poseen permisos de proyecto
        context['rol_list'] = Group.objects.all().exclude(~Q(permissions__content_type__model="permiso"))
        return context

    def get_success_url(self):
        id_proyecto_ = self.object.proyecto.id
        return reverse_lazy("proyecto:miembro:listar", kwargs={'pk': id_proyecto_})


"""
el miembro esta autorizado puede ver
la lista de miembros del proyecto
en el cual especifica el usuario,
el rol que posee en el proyecto,
las horas que trabaja

ademas se modifican los metodos 
query_set, get_context_data 
para personalizar y mantener 
los datos en estado correcto
"""


class ListarMiembro(ListView):
    model = Miembro
    template_name = 'miembro/Listar_Miembro.html'
    paginate_by = 10

    def get_queryset(self):
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        return Miembro.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context


"""
el miembro esta autorizado puede
 eliminar miembros del proyecto

ademas se modifican los metodos
 get_object y get_success_url
para personalizar y mantener 
los datos en estado correcto
"""


class EliminarMiembro(DeleteView):
    model = Miembro
    template_name = 'miembro/Eliminar_miembro.html'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['ide'])
        obj = queryset.get()
        return obj

    def get_success_url(self):
        idProyecto = self.kwargs['pk']
        return reverse_lazy("proyecto:miembro:listar", kwargs={'pk':idProyecto})

    def get_context_data(self, **kwargs):
        context = super(DeleteView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context



"""
el miembro esta autorizado puede
 modificar miembros del poryecto
ya sea el usuario,
el rol que posee en el proyecto,
las horas que trabaja

ademas se modifican los metodos 
get_object, get_context_data y
get_success_url para personalizar y 
mantener los datos en estado correcto
"""

class ModificarMiembro(UpdateView):
    model = Miembro
    fields = ['rolProyecto', 'horasDeTrabajo']
    template_name = 'miembro/Modificar_Miembro.html'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['id_'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        context['rol_list'] = Group.objects.all().exclude(~Q(permissions__content_type__model="permiso"))
        return context

    def get_success_url(self):
        idProyecto = self.kwargs['pk']
        return reverse_lazy("proyecto:miembro:listar", kwargs={'pk': idProyecto})

"""
se utiliza y modifica la funcion has_perm para verificar los permisos del usuario
"""


def has_perm(user, perm, proy_id):
    has = user.has_perm(perm)
    has |= user.has_perm(perm, Proyecto.objects.get(id=proy_id))
    return has

""""
funcion ver_productBacklog verifica 
permisos para ver el product backlog:
* si el miembro esta autorizado se le da 
acceso para ver el product backlog del Proyecto
* si no tiene esos permisos se le
 informa que no posee permisos
"""
@login_required()
def ver_productBacklog(request, *args, **kwargs):

    # `Return:` HttpResponse de acuerdo al caso.
    has_permission = request.user.has_perm('proyecto.ver_productBacklog')
    print("h product backlog ver")
    print(has_permission)
    has_permission |= request.user.has_perm('proyecto.ver_productBacklog', Proyecto.objects.get(id=kwargs['pk']))
    print(has_permission)
    if has_permission:
        return ProductBacklog.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><h1><p>No posee permisos</p></h1></html>")


"""
el miembro que esta autorizado puede 
visualizar el product backlog
ya sea el usuario,
el rol que posee en el proyecto,
las horas que trabaja

ademas se modifican los metodos 
get_queryset, get_context_data 
para personalizar y mantener 
los datos en estado correcto
"""


class ProductBacklog(ListView):
    model = US
    template_name = 'proyecto/ProductBacklog.html'
    paginate_by = 10

    def get_queryset(self):
        """
        `Override:` personaliza la lista de user stories a fin de proyectar solo los
        pertenecientes al proyecto.
        """
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs.get('pk'))
        return US.objects.filter(proyecto=self.proyecto)

    def get_context_data(self, **kwargs):
        """
        `Override:` agrega mas informacion para complementar los datos del menu.
        """
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        us_list = US.objects.filter(proyecto=context['proyecto'])
        context['us_list'] = us_list
        return context
