from django.conf.urls import url
from django.urls import path

from apps.miembros.views import verifica_permiso_listar, verifica_permiso_agregar, verifica_permiso_modificar, verifica_permiso_eliminar, ver_productBacklog


app_name = 'miembro'


urlpatterns = [
    path('', verifica_permiso_listar, name='listar'),
    url(r'^agregar$', verifica_permiso_agregar, name='agregar'),
    url(r'^modificar/(?P<id_>\d+)$', verifica_permiso_modificar, name='modificar'),
    url(r'^eliminar/(?P<ide>\d+)/$', verifica_permiso_eliminar, name='eliminar'),
   # url(r'^eliminar/(?P<ide>\d+)', EliminarMiembro.as_view(), name='eliminar'),

    url(r'^Product Backlog/$', ver_productBacklog, name='productBacklog'),
]
