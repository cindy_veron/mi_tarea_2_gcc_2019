from django.contrib.auth.models import User, Permission, Group
from django.db import models
from guardian.shortcuts import get_perms, remove_perm, assign_perm

#from apps.rolDeProyecto.models import RolDeProyecto
from apps.proyecto.models import Proyecto

"""
Se crea el Modelo Miembro, en el se guardan 
los datos del un miembro de Proyecto
usuario: como el id foraneo de usuario, 
proyecto: el proyecto al cual pertenece,
rolProyecto: el rol en el proyecto,
horasDeTrabajo: las horas de trabajo realiza 
"""

"""
Se modifica la funcion save para operaciones 
de creacion, actualizacion de datos
"""

"""
Se modifica la funcion de delete para eliminar
el permiso del miembro en el proyecto
y dejar en un estado valido
"""


class Miembro(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    rolProyecto = models.ForeignKey(Group, on_delete=models.SET_NULL,null=True)
    horasDeTrabajo = models.PositiveIntegerField(default=1)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        print(" esto es en save .self:"+str(self))
        old = Miembro.objects.filter(id=self.id)
        if old.count() > 0:
            print("old count >0")
            print(old.count())
            for x in get_perms(self.usuario, self.proyecto):
                print("Remover " + x + " a " + self.usuario.username + " en " + self.proyecto.nombre)
                remove_perm(x, self.usuario, self.proyecto)
        super().save(force_insert=force_insert,
                     force_update=force_update,
                     using=using,
                     update_fields=update_fields)
        rolenProyecto = self.rolProyecto
        print("self")
        print(self.rolProyecto)
        print("rol")
        print(rolenProyecto)
        if rolenProyecto == None:
            print("entro en none de rol")
            x = Permission.objects.get(codename="view_proyecto")
            print("Otorgar " + x.codename + " a " + self.usuario.username + " en " + self.proyecto.nombre)
            assign_perm(x.codename, self.usuario, self.proyecto)
        else:
            print("entro el else")
            print("rol en proyecto permissions all")
            print(rolenProyecto.permissions.all())
            for x in rolenProyecto.permissions.all():
                print("El valor de x")
                print(x)
                if x.content_type.name == 'permiso':
                    print("Otorgar " + x.codename + " a " + self.usuario.username + " en " + self.proyecto.nombre)
                    assign_perm(x.codename, self.usuario, self.proyecto)
                    print("asigno a usuario" + x.codename)
                    var = self.usuario.has_perm(x.codename,self.proyecto.nombre)
                    print(var)

    def delete(self, *args, **kwargs):
        print("llego a delete del modelo")
        old = Miembro.objects.filter(id=self.id)
        if old.count() > 0:
            for x in get_perms(self.usuario, self.proyecto):
                print("Remover " + x + " a " + self.usuario.username + " en " + self.proyecto.nombre)
                remove_perm(x, self.usuario, self.proyecto)
        super().delete(*args, **kwargs)

    def __str__(self):
        try:
            usuario=self.usuario.username
        except:
            usuario=""
        try:
            rol_asignado=self.rolProyecto.name
        except:
            rol_asignado="?"
        try:
            proyecto_in=self.proyecto.nombre
        except:
            proyecto_in=""

        return "%s" %(usuario)

    class Meta:
            unique_together = (('usuario', 'proyecto'),)
            ordering = ["id","rolProyecto","proyecto", "usuario"]
