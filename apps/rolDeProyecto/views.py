from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, UpdateView, DeleteView, CreateView
from django.contrib.auth.models import Group, Permission
from django.urls import reverse_lazy
from django.shortcuts import render, redirect, render_to_response

from apps.miembros.views import has_perm
from apps.rolDeProyecto.forms import GroupForm
from apps.proyecto.models import Proyecto

"""
    funcion verifica_permisos de agregar rol:
    *si el miembro esta autorizado se le da 
        acceso agregar crear rol del proyecto
    *si no tiene esos permisos se le informa 
        que no posee permisos

"""

@login_required()
def verifica_permiso_agregar(request,*args,**kwargs):
    if has_perm(request.user, 'proyecto.crear_rol_proyecto', kwargs['pk']):
        return CrearRolDeProyecto.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


"""
    funcion verifica_permisos de modificar rol:
    *si el miembro esta autorizado se le da 
        acceso modificar roles de proyecto
    *si no tiene esos permisos se le informa 
        que no posee permisos

"""

@login_required()
def verifica_permiso_modificar(request,*args,**kwargs):
    if has_perm(request.user, 'proyecto.modificar_rol_proyecto', kwargs['pk']):
        return ModificarRolDeProyecto.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


"""
    funcion verifica_permisos de listar roles:
    *si el miembro esta autorizado se le da 
        acceso a ver roles de proyecto
    *si no tiene esos permisos se le informa 
        que no posee permisos

"""

@login_required()
def verifica_permiso_listar(request,*args,**kwargs):
    if has_perm(request.user, 'proyecto.ver_rol_proyecto', kwargs['pk']):
        return ListarRolDeProyecto.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")

"""
    funcion verifica_permisos de eliminar rol:
    *si el miembro esta autorizado se le da 
        acceso eliminar roles de proyecto
    *si no tiene esos permisos se le informa 
        que no posee permisos

"""

@login_required()
def verifica_permiso_eliminar(request,*args,**kwargs):
    if has_perm(request.user, 'proyecto.eliminar_rol_proyecto', kwargs['pk']):
        return EliminarRolDeProyecto.as_view()(request, *args, **kwargs)
    else:
        return HttpResponse("<html><p>No posee permisos</p></html>")


"""
    Clase CrearRolDeProyecto:
    Hereda de CreateView
    
    el miembro esta autorizado puede agregar 
    rol de Proyecto 
    
    Se sobreescriben los metodos get_context_data 
    y get_success_url para personalizar y mantener 
    los datos en estado correcto

"""
class CrearRolDeProyecto(CreateView):
    model = Group
    form_class = GroupForm
    template_name = "rolDeProyecto/rol_proyecto_crear.html"

    def get_context_data(self, **kwargs):
        #sobreescibe el contexto
        context = super(CreateView, self).get_context_data(**kwargs)
        #crea el contexto proyecto con la id de proyecto actual
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context
    def get_success_url(self):
       # print(str(self.object.proyecto.id)+" en get success url")
        id_proyecto_ =  self.kwargs['pk'] #Proyecto.objects.get(id=self.kwargs['pk'])
        return reverse_lazy("proyecto:rolProyecto:listar", kwargs={'pk': id_proyecto_})


"""
    Clase Listar rol de Proyecto:
    parametros:
    1. PermissionRequiredMixin: CBV mixin que verifica que 
        el usuario actual tiene todos los permisos especificados
    2. ListView: Renderiza alguna lista de objetos.
    **Retorna:** url a listar_rol

    Utiliza requerimiento de permiso para verificar si el 
    usuario tiene permiso de listar rol de proyecto
    
    Se sobreescriben los metodos get_queryset 
    y get_context_data para personalizar y mantener 
    los datos en estado correcto
"""


class ListarRolDeProyecto(ListView):
    model = Group
    template_name = 'rolDeProyecto/rol_proyecto_listar.html'

    def get_queryset(self):
        self.proyecto = get_object_or_404(Proyecto, id=self.kwargs['pk'])
        grupos = Group.objects.all().exclude(~Q(permissions__content_type__model="permiso"))
        return grupos
    def get_context_data(self, **kwargs):
        context = super(ListView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        #for i in ['add', 'edit', 'delete']:
         #   context['perm_'+i] = has_perm(self.request.user, "proyectos."+i+" Miembro ", self.kwargs["pk"])
        return context


"""
    Clase Eliminar rol de Proyecto:
    parametros:
    1. PermissionRequiredMixin: CBV mixin que verifica que 
        el usuario actual tiene todos los permisos especificados
    2. DelteView: Renderiza alguna lista de objetos.
    **Retorna:** url a listar_rol

    Utiliza requerimiento de permiso para verificar si el 
    usuario tiene permiso de listar rol de proyecto
    
    Se sobreescriben los metodos get_success_url,
    get_context_data y get_object para personalizar 
    y mantener los datos en estado correcto
"""


class EliminarRolDeProyecto(DeleteView):
    model = Group
    template_name = 'rolDeProyecto/rol_proyecto_eliminar.html'

    def get_success_url(self):
        idProyecto = self.kwargs['pk']
        return reverse_lazy("proyecto:rolProyecto:listar", kwargs={'pk':idProyecto})

    def get_context_data(self, **kwargs):
        context = super(DeleteView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        return context

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['id'])
        obj = queryset.get()
        return obj


"""
    Clase Modificar rol de Proyecto:
    parametros:
    1. PermissionRequiredMixin: CBV mixin que verifica que 
        el usuario actual tiene todos los permisos especificados
    2. UpdateView: Renderiza alguna lista de objetos.
    **Retorna:** url a listar_rol

    Utiliza requerimiento de permiso para verificar si el 
    usuario tiene permiso de listar rol de proyecto
    
    Se sobreescriben los metodos get_success_url,
    get_context_data y get_object para personalizar 
    y mantener los datos en estado correcto
"""


class ModificarRolDeProyecto(UpdateView):
    model = Group
    form_class = GroupForm
    template_name = 'rolDeProyecto/rol_proyecto_modificar.html'

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(id=self.kwargs['id'])
        obj = queryset.get()
        return obj

    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data(**kwargs)
        context['proyecto'] = Proyecto.objects.get(id=self.kwargs['pk'])
        print(context)
        return context

    def get_success_url(self):
        idProyecto = self.kwargs['pk']
        return reverse_lazy("proyecto:rolProyecto:listar", kwargs={'pk': idProyecto})
