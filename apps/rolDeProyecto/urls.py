from django.urls import path
from django.conf.urls import url
from apps.rolDeProyecto.views import verifica_permiso_listar, verifica_permiso_eliminar, verifica_permiso_modificar, verifica_permiso_agregar

app_name = 'rolProyecto'

urlpatterns = [
    url(r'^$', verifica_permiso_listar, name='listar'),
    url(r'^crear', verifica_permiso_agregar, name='crear'),
    url(r'^modificar/(?P<id>\d+)$', verifica_permiso_modificar, name='modificar'),
    url(r'^eliminar/(?P<id>\d+)$', verifica_permiso_eliminar, name='eliminar'),
]
