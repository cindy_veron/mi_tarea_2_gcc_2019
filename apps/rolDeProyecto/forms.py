
from django.contrib.auth.models import Group,Permission
from django import forms
from django.db.models import Q


"""
    Formulario:
    
    hereda de forms.Modelform
    
    permissions: se filtran los permisos para 
    rol de proyecto
    
    Class Meta:
    se especifican los campos que seran que
    seran añadidos o modificados:
    *name,
    *permissions
"""


class GroupForm(forms.ModelForm):
    #Todos los permisos listados pertenecen a los permisos creados en el meta del modelo Proyecto
    permissions = forms.ModelMultipleChoiceField(queryset=Permission.objects.filter(
                                                            Q(content_type__model="permiso")),
                                                            required=True
                                                )

    class Meta:
        model = Group
        fields = [
            'name',
            'permissions'
        ]
        order_with_respect_to = 'Permissions'

#.exclude(content_type_id=id_).exclude(content_type_id=id_).exclude(content_type_id=id_)
